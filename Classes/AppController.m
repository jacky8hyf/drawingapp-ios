/*
     File: AppController.m
 Abstract: The UIApplication delegate class.
  Version: 1.13
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 
*/

#import "AppController.h"
#import "PaintingViewController.h"
#import "RBCanvasService.h"

@implementation AppController

+ (instancetype)sharedInstance
{
    return (AppController *)[[UIApplication sharedApplication] delegate];
}

- (void)applicationDidFinishLaunching:(UIApplication*)application
{
    [AVOSCloud setApplicationId:@"dnpcv05kbk60qwdb9ye4iue1egkoxbb1pl5enhigcmgn5mjr"
                      clientKey:@"clzyujd6z72dg6er5c3p1iizbj6sc4hz0v00zrkqg6t9bz52"];
    self.deviceTokenFetchState = kRBStateInitializing;
    // https://leancloud.cn/docs/ios_push_guide.html#保存_Installation
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert
                                                | UIUserNotificationTypeBadge
                                                | UIUserNotificationTypeSound
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // iOS 7 campatibility
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [application registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeSound];
#pragma clang diagnostic pop
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    self.deviceTokenFetchState = kRBStateSuccess;
    NSLog(@"[AppDelegate] deviceToken = %@", deviceToken);
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        NSLog(@"[AppDelegate] success = %d in saving device token to background, %@", succeeded, error);
        if(!succeeded) {
            RBToast(RBToastNoLeancloudLocalized);
        }
#if DEBUG
        else {
            RBToast(RBToastHasPushLocalized);
        }
#endif
    }];
}
- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    self.deviceTokenFetchState = kRBStateError;
    if(error.code == 3010) {
        NSLog(@"[AppDelegate] cannot connect to push server on a simulator: %@", error);
    }
    RBToast(RBToastNoPushLocalized);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    id action = [userInfo objectForKey:RBLeancloudPushActionKey];
    if([action isKindOfClass:[NSString class]] && [action length]) {
        if([action rangeOfString:RBLeancloudPushActionStrokeAdded options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [self performSelectorInBackground:@selector(downloadStrokes:) withObject:@[userInfo,handler]];
            return;
        } else if ([action rangeOfString:RBLeancloudPushActionStrokeRemoved options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [self performSelectorInBackground:@selector(removeStroke:) withObject:@[userInfo, handler]];
            return;
        }
    }
    NSLog(@"[AppDelegate] unknown push notification. userInfo = %@", userInfo);
    handler(UIBackgroundFetchResultFailed);
}

- (void)downloadStrokes:(NSArray *)args {
    NSDictionary *userInfo = [args objectAtIndex:0];
    void (^handler)(UIBackgroundFetchResult) = [args objectAtIndex:1];
    id createdAt = [userInfo objectForKey:RBLeanCloudPushCreatedAtKey]; // createdAt is in millis
    if(createdAt && [createdAt isKindOfClass:[NSNumber class]]) {
        NSTimeInterval timeSeconds = ([createdAt longLongValue] / 1000.);
        [RBCanvasService.instance asyncDownloadAndDrawStrokesTill:timeSeconds];
    }
    handler(UIBackgroundFetchResultNoData);
}

- (void)removeStroke:(NSArray *)args {
    NSDictionary *userInfo = [args objectAtIndex:0];
    void (^handler)(UIBackgroundFetchResult) = [args objectAtIndex:1];
    id strokeId = [userInfo objectForKey:RBLeanCloudPushStrokeIdKey];
    if([strokeId isKindOfClass:[NSString class]] && [strokeId length])
        [RBCanvasService.instance onServerRemovedStrokeWithId:strokeId];
    handler(UIBackgroundFetchResultNoData);
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    NSLog(@"[AppDelegate]Calling Application Bundle ID: %@", sourceApplication);
    NSLog(@"[AppDelegate]URL scheme:%@", [url scheme]);
    NSLog(@"[AppDelegate]URL query: %@", [url query]);
    NSLog(@"[AppDelegate]URL host: %@", [url host]);
    NSLog(@"[AppDelegate]URL path: %@", [url path]);
    
    NSString *canvasId = parseShareURL(url);
    if(canvasId.length)
        [RBCanvasService.instance openCanvas:canvasId];
    return canvasId.length;
}


@end
