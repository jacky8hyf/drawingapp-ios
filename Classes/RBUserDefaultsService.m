//
//  RBUserDefaultsService.m
//  drawingapp
//
//  Created by Elsk Hong on 7/11/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import "RBUserDefaultsService.h"

@implementation RBUserDefaultsService

SHARED_INSTANCE_ACCESS_METHOD(instance);

- (instancetype) init {
    self = [super init];
    if(self) {
        _canvasIds = nil;
        _mutex = [NSObject new];
        _userDef = nil;
    }
    return self;
}

- (void) initUserDefWithError:(NSError **)error {
    if(!_userDef)
        _userDef = [NSUserDefaults standardUserDefaults];
    if(!_userDef) {
        *error = [NSError errorWithDomain:RBErrorDomain
                                     code:kRBErrorCannotLoadUserDefaults
                                 userInfo:@{NSLocalizedDescriptionKey:RBErrorCannotLoadUserDefaultsMessage}];
        return;
    }
}
- (void) loadWithError:(NSError **)error {
    @synchronized(_mutex) {
        if(_canvasIds)
            return;
        [self initUserDefWithError:error];
        if(*error) return;
        _canvasIds = [[NSMutableOrderedSet alloc] initWithArray:[_userDef objectForKey:RBUserDefaultsFavoritesKey]];
        if(_canvasIds == nil)
            _canvasIds = [NSMutableOrderedSet new];
    }
}
- (void) saveWithError:(NSError **)error {
    @synchronized(_mutex) {
        [self initUserDefWithError:error];
        if(*error) return;
        [_userDef setObject:[_canvasIds array] forKey:RBUserDefaultsFavoritesKey];
        if(![_userDef synchronize])
            *error = [NSError errorWithDomain:RBErrorDomain
                                         code:kRBErrorCannotSaveUserDefaults
                                     userInfo:@{NSLocalizedDescriptionKey:RBErrorCannotSaveUserDefaultsMessage}];
    }
}

- (BOOL) toggleFavoriteOfCanvasId:(NSString *)canvasId error:(NSError **)error {
    if(!canvasId.length) {
        *error = [NSError errorWithDomain:RBErrorDomain
                                     code:kRBErrorEmptyCanvasId
                                 userInfo:@{NSLocalizedDescriptionKey:RBErrorEmptyCanvasIdMessage}];
        return NO;
    }
    
    @synchronized(_mutex) {
        [self loadWithError:error];
        if(*error) return NO;
        BOOL res;
        if([_canvasIds containsObject:canvasId]) {
            [_canvasIds removeObject:canvasId];
            res = NO;
        } else {
            [_canvasIds addObject:canvasId];
            res = YES;
        }
        [self saveWithError:error];
        if(*error) {
            // rollback changes
            if(res) [_canvasIds removeObject:canvasId];
            else [_canvasIds addObject:canvasId];
            return !res;
        }
        return res;
    }
}
- (BOOL) isFavorite:(NSString *)canvasId error:(NSError **)error {
    if(!canvasId.length) {
        *error = [NSError errorWithDomain:RBErrorDomain
                                     code:kRBErrorEmptyCanvasId
                                 userInfo:@{NSLocalizedDescriptionKey:RBErrorEmptyCanvasIdMessage}];
        return NO;
    }
    @synchronized(_mutex) {
        [self loadWithError:error];
        if(*error) return NO;
        return [_canvasIds containsObject:canvasId];
    }
}
- (NSArray * /*of NSString*/ ) allFavoriteCanvasesWithError:(NSError **)error {
    @synchronized(_mutex) {
        [self loadWithError:error];
        if(*error) return nil;
        return [_canvasIds array];
    }
}
@end
