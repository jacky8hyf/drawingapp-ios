//
//  RBBezierPath.m
//  drawingapp
//
//  Created by Elsk Hong on 7/14/15.
//
//

#import "RBBezierPath.h"

@implementation RBBezierPath

- (instancetype) initWithStartPoint:(CGPoint)start {
    if((self = [super init])) {
        _currentPoint = _startPoint = start;
        _capacity = RBBezierPathSegmentsInitialSize;
        _segments = malloc(_capacity * sizeof(RBBezierSegment));
        if(!_segments)
            return nil;
        _size = 0;
        _xMin = CGFloatFloor(start.x);
        _xMax = CGFloatCeil(start.x);
        _yMin = CGFloatFloor(start.y);
        _yMax = CGFloatCeil(start.y);
        _bitmap = nil;
//        _bitmapSize = 0;
        _bitmapWidth = 0;
        _bitmapHeight = 0;
    }
    return self;
}

+ (instancetype) bezierPathWithStartPoint:(CGPoint)start {
    return [[self alloc] initWithStartPoint:start];
}

- (void) lineTo:(CGPoint)end {
    [self ensureCapacity:_size + 1];
    _segments[_size] = RBBezierSegmentLineMake(end);
    _size++;
    _currentPoint = end;
    
    // comparisons between two NSInteger is fine.
    _xMin = MIN(_xMin, CGFloatFloor(end.x));
    _xMax = MAX(_xMax, CGFloatCeil(end.x));
    _yMin = MIN(_yMin, CGFloatFloor(end.y));
    _yMax = MAX(_yMax, CGFloatCeil(end.y));
    
    if(_bitmap) {
        free(_bitmap);
        NSLog(@"[RBBitmap] freeing bitmap of %ld bytes", (long)((_bitmapHeight * _bitmapWidth + 7) / 8));
        _bitmap = nil;
        _bitmapWidth = 0;
        _bitmapHeight = 0;
    }
}

- (void) quadTo:(CGPoint)end controlPoint:(CGPoint)control {
    [self ensureCapacity:_size + 1];
    _segments[_size] = RBBezierSegmentQuadMake(control, end);
    _size++;
    _currentPoint = end;
    
    // comparisons between two NSInteger is fine.
    _xMin = MIN(MIN(CGFloatFloor(control.x), CGFloatFloor(end.x)), _xMin);
    _xMax = MAX(MAX(CGFloatCeil (control.x), CGFloatCeil(end.x) ), _xMax);
    _yMin = MIN(MIN(CGFloatFloor(control.y), CGFloatFloor(end.y)), _yMin);
    _yMax = MAX(MAX(CGFloatCeil (control.y), CGFloatCeil(end.y) ), _yMax);
    
    if(_bitmap) {
        free(_bitmap);
        NSLog(@"[RBBitmap] freeing bitmap of %ld bytes", (long)((_bitmapHeight * _bitmapWidth + 7) / 8));
        _bitmap = nil;
        _bitmapWidth = 0;
        _bitmapHeight = 0;
    }
}

- (void) enumerateSegmentsUsingBlock:(void (^)(RBBezierSegmentRef, NSUInteger, BOOL *))block
{
    BOOL stop;
    for (NSUInteger i = 0; i < _size; i++) {
        block(&_segments[i], i, &stop);
        if(stop)break;
    }
}

/*!
 * Set _bitmap to a bitmap of the graph. If after calling this function
 * _bitmap is still nil, then compare _currentPoint for intersections.
 */
- (void) createBitmap {
    if(_bitmap)
        return;
#if DEBUG
//    swBegin();
#endif
    NSUInteger width = _xMax - _xMin + 1, height = _yMax - _yMin + 1, numBits = width * height;
    if(numBits <= 0)
        return;
    _bitmapWidth = width;
    _bitmapHeight = height;
    _bitmap = RBBitmapCreate(numBits);
    [self drawPathUsingLineDrawer:^(CGPoint start, CGPoint end) {
        CGFloat t;
        NSUInteger i, xt, yt,
        count = linearCount(start, end, 1);
        for(i = 0; i < count; i++) {
            t = ((CGFloat)i) / ((CGFloat)count);
            xt = CGFloatRound(linear(start, end, x, t)) - _xMin;
            yt = CGFloatRound(linear(start, end, y, t)) - _yMin;
            if(!RBBitmapSet(_bitmap, _bitmapWidth, _bitmapHeight, xt, yt)) {
                NSLog(@"[RBBezierPath] FATAL: cannot set! (%d x %d), (%d, %d)", (int)_bitmapWidth, (int)_bitmapHeight, (int)xt, (int)yt);
            }
        }
    } quadDrawer:^(CGPoint start, CGPoint control, CGPoint end) {
        CGFloat t;
        NSUInteger i, x, y,
        count = quadCount(start, control, end, 1);
        for(i = 0; i < count; i++) {
            t = ((CGFloat)i) / ((CGFloat)count); // not count - 1, just leave the end point blank; the "1. *" is emitted.
            x = CGFloatRound(quad(start, control, end, x, t)) - _xMin;
            y = CGFloatRound(quad(start, control, end, y, t)) - _yMin;
            if(!RBBitmapSet(_bitmap, _bitmapWidth, _bitmapHeight, x, y)) {
                NSLog(@"[RBBezierPath] FATAL: cannot set! (%d x %d), (%d, %d)", (int)_bitmapWidth, (int)_bitmapHeight, (int)x, (int)y);
            }
        }
    }];
#if DEBUG
//    swCheck(@"CBm");
//    [self dumpBitmap];
#endif
}

- (BOOL) intersectWithCircleAt:(CGPoint)realCenter radius:(CGFloat)radius
{
    CGPoint center = realCenter;
    // shift center
    center.x -= _xMin;
    center.y -= _yMin;
    
    RBCoordInt xs = CGFloatFloor(center.x - radius);
    RBCoordInt xe = CGFloatCeil (center.x + radius);
    RBCoordInt ys = CGFloatFloor(center.y - radius);
    RBCoordInt ye = CGFloatCeil (center.y + radius);
    // this four variables constructs the circle's square boundary.
    // sanity check
    if(xs > xe || ys > ye)
        return NO;
    
    // comparisons between two NSInteger is fine.
    // check if this square is within the rectangle 0, _xMax - _xMin, 0, _yMax - _yMin
    if(xe < 0 || xs > _xMax - _xMin || ye < 0 || ys > _yMax - _yMin)
        return NO;
    
    // it is within the rectangle. Let's create the bitmap and check.
    [self createBitmap];
    if(!_bitmap) {
        // no bitmap for this curve, so do a normal distance check.
        return CGPointDist(realCenter, _currentPoint) < radius;
    }
    
    CGFloat squaredRadius = square(radius);
    for(RBCoordInt x = xs; x <= xe; x++) {
        // this is a misnomer; this is really deltaY squared. Here just to reuse the variable later.
        CGFloat deltaY/* Squared */ = squaredRadius - square(x - center.x);
        // because of xs / xe / ys / ye's floor and ceiling thing, the square is a
        // little bit larger than the circle; hence deltaY^2 can be negative.
        if(deltaY < 0.)
            continue;
        deltaY = CGFloatSqrt(deltaY);
        ys = CGFloatFloor(center.y - deltaY);
        ye = CGFloatCeil (center.y + deltaY);
        for(RBCoordInt y = ys; y <= ye; y++) {
            if(RBBitmapGet(_bitmap, _bitmapWidth, _bitmapHeight, x, y)) {
#if DEBUG
                CGFloat dist = CGPointDist(CGPointMake(x + _xMin, y + _yMin), realCenter);
                NSLog(@"[RBBezierPath] (%d,%d) intersects with circle (%.1f,%.1f) x %.1f; actual dist = %f", (int)(x + _xMin), (int)(y + _yMin), realCenter.x, realCenter.y, radius, dist);
#endif
                return YES; // TODO potential byte comparison here.
            }
        }
    }
    return NO;
}

#if DEBUG
- (BOOL) dumpBitmap {
    if(!_bitmap)
        return NO;
    for (NSUInteger y = 0; y < _bitmapHeight; y++) {
        NSMutableString *str = [NSMutableString stringWithCapacity:_bitmapWidth];
        for(NSUInteger x = 0; x < _bitmapWidth; x++) {
            [str appendFormat:@"%d", RBBitmapGet(_bitmap, _bitmapWidth, _bitmapHeight, x, y) ? 1 : 0];
        }
        NSLog(@"%@", str);
    }
    return YES;
}
#endif

- (void) drawPathUsingLineDrawer:(void (^)(CGPoint start, CGPoint end))lineDrawer
                      quadDrawer:(void (^)(CGPoint start, CGPoint control, CGPoint end))quadDrawer
{
    CGPoint p = _startPoint;
    
    for (NSUInteger i = 0; i < _size; i++) {
        RBBezierSegmentRef seg = &_segments[i];
        
        switch (seg->action) {
            case kRBBezierPathLine:
                lineDrawer(p, seg->end);
                p = seg->end;
                break;
            case kRBBezierPathQuad:
                quadDrawer(p, seg->control, seg->end);
                p = seg->end;
                break;
            default:
                NSLog(@"[RBBezierPath] FATAL: does not know action %d", seg->action);
                break;
        }
    }
    // to mimic the exact behavior described in PaintingView touches... and RBStroke finish,
    // I need to add "draw-endpoint" here.
    lineDrawer(p, p);
}

- (void)ensureCapacity:(NSUInteger)requiredSize {
    if(_capacity >= requiredSize)
        return;
    NSUInteger newSize = _capacity << 1;
    while(newSize < requiredSize) {
        if(newSize <= _capacity)
            @throw [NSException exceptionWithName:@"IntegerOverflow" reason:@"integer overflow" userInfo:nil];
        newSize <<= 1;
    }
    RBBezierSegment *buf = realloc(_segments, newSize * sizeof(RBBezierSegment));
    if(!buf)
        @throw [NSException exceptionWithName:@"OutOfMemoryError" reason:@"cannot allocate memory" userInfo:nil];
    _segments = buf;
    _capacity = newSize;
}

- (void)dealloc {
    if(_segments) {
        free(_segments);
        _segments = nil;
    }
    if(_bitmap) {
        free(_bitmap);
        NSLog(@"[RBBitmap] freeing bitmap of %ld bytes", (long)((_bitmapWidth * _bitmapHeight + 7) / 8));
        _bitmap = nil;
        _bitmapWidth = 0;
        _bitmapHeight = 0;
    }
}


@end
