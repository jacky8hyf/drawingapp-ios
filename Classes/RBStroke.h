//
//  RBStroke.h
//  drawingapp
//
//  Created by Elsk Hong on 7/5/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#ifndef drawingapp_RBStroke_h
#define drawingapp_RBStroke_h

#import "RBBezierPath.h"

@interface RBStroke : NSObject {
    /* final */ NSObject * _mutex;
    /* final */ NSMutableArray * /* of NSArray of NSNumber */ _points;
    /*!
     * A good approximation of a curve that
     * goes through all points in _points.
     */
    /* final */ RBBezierPath * _bezierPath;
}
@property (readonly, atomic) BOOL finished;
@property (readwrite, nonatomic) NSString *strokeId;

//@property (readonly, nonatomic) UIBezierPath * path;


/*!
 * Return a finished stroke that goes through the
 * given points. This method is recommended to be called on a background thread,
 * since it is computationally heavy.
 * @return nil if cannot deseiralize points, else an RBStroke object
 * from these points.
 */
+ (instancetype) strokeFromPoints:(id)points withId:(NSString *)strokeId;

/*!
 * Create a new stroke that starts from the given point.
 * This point will be added to the _points array.
 */
+ (instancetype) strokeWithPoint:(CGPoint)startPoint;

/*!
 * Add a point to the _points array and interpolate some part of
 * _path (not necessary to newPoint)
 */
- (void) addPoint:(CGPoint)newPoint;

/*!
 * Add a point to the _points array and interpolate some part of
 * _path (not necessary to newPoint)
 */
- (BOOL) addPoint:(CGPoint)newPoint
 returnStartPoint:(CGPoint *)startPointPtr
     controlPoint:(CGPoint *)controlPointPtr
         endPoint:(CGPoint *)endPointPtr;

/*!
 * Finishes up this stroke, and extend _path to the last
 * point added. From now on addPoint: cannot be called.
 */
- (void) finish;

- (BOOL) finishAndReturnStartPoint:(CGPoint *)startPointPtr
                          endPoint:(CGPoint *)endPointPtr
                      createBitmap:(BOOL)createBitmap;

/*!
 * After finish was called, this method can properly return a copy
 * of all points in order. (Since it involves copying and no memorization
 * do not call this method a lot of times)
 */
- (NSArray *) pointsArray;

/*!
 * Must be invoked after finish was called.
 * @return whether the bezier path intersects with the given solid circle.
 */
- (BOOL) intersectWithCircleAt:(CGPoint)center radius:(CGFloat)radius;

/*!
 * Delegate to _bezierPath's method call.
 */
- (void) drawPathUsingLineDrawer:(void (^)(CGPoint start, CGPoint end))lineDrawer
                      quadDrawer:(void (^)(CGPoint start, CGPoint control, CGPoint end))quadDrawer;
@end

#endif
