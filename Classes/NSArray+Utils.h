//
//  NSArray+Utils.h
//  drawingapp
//
//  Created by Elsk Hong on 7/11/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#ifndef drawingapp_NSArray_Utils_h
#define drawingapp_NSArray_Utils_h

@interface NSArray(RBUtils)

// http://fuckingblocksyntax.com/
/*!
 * @return an array that:
 * 1. has the same length of the receiver;
 * 2. has element at index i being func([self objectAtIndex:i]).
 */
- (NSArray *)arrayByApplyingFunction:(id (^)(id))func;
@end

#endif
