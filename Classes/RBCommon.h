//
//  RBCommon.h
//  drawingapp
//
//  Created by Elsk Hong on 7/6/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#ifndef drawingapp_RBCommon_h
#define drawingapp_RBCommon_h

#import <mach/mach_time.h>

#define RBString(key) NSLocalizedString(key, nil)

// ~--- constants
#define NANOSECONDS_PER_MILLISECOND NSEC_PER_MSEC
#define NANOSECONDS_PER_SECOND NSEC_PER_SEC
/*! 
 * The application-wise logical width of a board. Height is undefined,
 * and varies from device to device. On iPad. height is 768.
 */
#define RBBoardWidth ((CGFloat)1024.)
#define RBBoardHeight ((CGFloat)768.)
#define RBBoardWHRatio (RBBoardWidth / RBBoardHeight)
/*!
 * The application-wise logical distance between two points such
 * that an addPoint will be triggered by an RBStroke.
 */
#define RBDrawing_DISTANCE_THRESHOLD 3.

// Menu & Dialog
#define RBMainMenuOpenCanvasKey @"Open Canvas"
#define RBMainMenuToggleFavoritesKey @"Toggle Favorite"
#define RBMainMenuFavoritesListKey @"Favorites"
#define RBMainMenuShareKey @"Share"
#define RBMainMenuToggleEraserKey @"Toggle eraser"
#define RBMainMenuHelpKey @"Help"
#define RBMainMenuAboutKey @"About"
#define RBDialogTitleCanvasIdLocalizedFormat RBString(@"Canvas ID is")
#define RBDialogTitleMenuLocalized RBString(@"Menu")
#define RBDialogTitleOpenCanvasLocalized RBString(RBMainMenuOpenCanvasKey)
#define RBDialogTitleAboutLocalized RBString(RBMainMenuAboutKey)
#define RBDialogTitleFavoritesLocalized RBString(RBMainMenuFavoritesListKey)
#define RBDialogInputCanvasIdLocalized RBString(@"Input canvas ID")
#define RBDialogAboutMessageLocalizedFormat RBString(@"About message")
#define RBDialogOkLocalized RBString(@"OK")
#define RBDialogOfficialWebsiteLocalized RBString(@"Official website")
#define RBDialogCancelLocalized RBString(@"Cancel")

// keys for NSNotificationCenter
#define RBNotificationToastName @"toast"
#define RBNotificationToastMessageKey @"message"

//#define RBNotificationCanvasProfileContentChangedName @"canvasProfileContentChanged"
//#define RBNotificationAddedStrokeName @"addedStroke"
//#define RBNotificationCannotAddStrokeName @"cannotAddStroke"

// keys for args in RBMessagesService
//#define RBNotificationErrorKey @"error"
//#define RBNotificationShouldToastIfSuccessKey @"shouldToast"
//#define RBNotificationStrokesAddedKey @"strokesAdded"
//#define RBNotificationStrokeIdKey @"strokeId"

// Leancloud class names
#define RBLeanCloud_STROKE_CLASS_NAME @"Stroke"
// Leancloud object attributes
#define RBLeanCloud_STROKE_POINTS @"points"
#define RBLeanCloud_STROKE_CANVAS_ID @"canvasId"
#define RBLeanCloudPushStrokeIdKey @"strokeId"
#define RBLeanCloudPushCreatedAtKey @"createdAt"
#define RBLeancloudPushActionKey @"action"
#define RBLeancloudPushActionStrokeAdded @"STROKE_ADDED"
#define RBLeancloudPushActionStrokeRemoved @"STROKE_REMOVED"

// key for user defaults
#define RBUserDefaultsFavoritesKey @"favoriteCanvasIds"

#define RBErrorDomain @"RBErrorDomain"
#define RBErrorCannotLoadUserDefaultsMessage RBString(@"Cannot load user defaults")
#define RBErrorCannotSaveUserDefaultsMessage RBString(@"Cannot save user defaults")
#define RBErrorEmptyCanvasIdMessage RBString(@"Empty Canvas Id")
#define RBToastAddedToFavoriteLocalizedFormat RBString(@"Added to favorite")
#define RBToastRemovedFromFavoriteLocalizedFormat RBString(@"Removed from favorite")
#define RBToastNoPushLocalized RBString(@"No push")
#define RBToastNoLeancloudLocalized RBString(@"No leancloud")
#define RBToastHasPushLocalized RBString(@"Has push")
#define RBToastEmptyFavoriteListLocalized RBString(@"Empty favorites")
#define RBToastLoadingCanvasLocalizedFormat RBString(@"Loading canvas")
#define RBToastCannotDownloadStrokesLocalizedFormat RBString(@"Cannot download strokes")
#define RBToastSuccessfullyDownloadedCanvasLocalizedFormat RBString(@"Downloaded canvas")
#define RBToastCannotAddStrokeLocalized RBString(@"Cannot add stroke")
#define RBToastCannotDeleteStrokeLocalized RBString(@"Cannot delete stroke")
#define RBToastUsingBrush RBString(@"Using brush")
#define RBToastUsingEraser RBString(@"Using eraser")
#define RBToastDumpingImage RBString(@"Dumping image")
// ~--- utility functions

// NS_INLINE is a pretty good inline definition.
#define RB_INLINE NS_INLINE

#if CGFLOAT_IS_DOUBLE
/*! Absolute value of a CGFloat */
#define CGFloatAbs fabs
#define CGFloatFloor(x) ((NSInteger)(floor(x)))
#define CGFloatCeil(x)  ((NSInteger)(ceil(x)))
#define CGFloatRound(x) ((NSInteger)(round(x)))
#define CGFloatSqrt sqrt
#else
/*! Absolute value of a CGFloat */
#define CGFloatAbs fabsf
#define CGFloatFloor(x) ((NSInteger)(floorf(x)))
#define CGFloatCeil(x)  ((NSInteger)(ceilf(x)))
#define CGFloatRound(x) ((NSInteger)(roundf(x)))
#define CGFloatSqrt sqrtf
#endif

#define square(d) (d) * (d)

/*! Wraps a struct CGPoint to an NSArray. This
 * is a function instead of a macro because value
 * will not be evaluated multiple times. */
RB_INLINE NSArray *
WRAP_POINT(const CGPoint point)
{
#if CGFLOAT_IS_DOUBLE
    return [NSArray arrayWithObjects:
            [NSNumber numberWithDouble:point.x],
            [NSNumber numberWithDouble:point.y], nil];
#else
    return [NSArray arrayWithObjects:
            [NSNumber numberWithFloat:point.x],
            [NSNumber numberWithFloat:point.y], nil];
#endif
}

/*! Unwraps an NSArray to a a struct CGPoint. This
 * is a function instead of a macro because value
 * will not be evaluated multiple times. */
RB_INLINE CGPoint
UNWRAP_POINT(const NSArray *value)
{
#if CGFLOAT_IS_DOUBLE
    return CGPointMake([[value objectAtIndex:0] doubleValue], [[value objectAtIndex:1] doubleValue]);
#else
    return CGPointMake([[value objectAtIndex:0] floatValue], [[value objectAtIndex:1] floatValue]);
#endif
}

RB_INLINE CGPoint
CGPointMakeFromNSNumbers(const NSNumber *x, const NSNumber *y)
{
#if CGFLOAT_IS_DOUBLE
    return CGPointMake([x doubleValue], [y doubleValue]);
#else
    return CGPointMake([x floatValue], [y floatValue]);
#endif
}

RB_INLINE CGFloat
CGPointDist(const CGPoint p, const CGPoint q) {
    return CGFloatSqrt(square(p.x - q.x) + square(p.y - q.y));
}

RB_INLINE CGPoint
CGPointScale(const CGPoint point, const CGFloat multiplyFactor)
{
    CGPoint p; p.x = point.x * multiplyFactor; p.y = point.y * multiplyFactor; return p;
}

// http://stackoverflow.com/a/7622902
RB_INLINE uint32_t
RBRandom32BitInteger(void)
{
    uint32_t x = rand() & 0xff;
    x |= (rand() & 0xff) << 8;
    x |= (rand() & 0xff) << 16;
    x |= (rand() & 0xff) << 24;
    return x;
}

RB_INLINE NSString*
RBVersion() {
    // http://stackoverflow.com/questions/10015304/refer-to-build-number-or-version-number-in-code
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
    NSNumber *buildNumber = [infoDict objectForKey:@"CFBundleVersion"]; // example: 42
    return [NSString stringWithFormat:@"%@ (%@)", appVersion, buildNumber];
}

// https://github.com/magicalpanda/MagicalRecord/issues/380#event-109920903
#define WEAK_SELF(self, weakSelf) __typeof(self) __weak weakSelf = self;

#define SHARED_INSTANCE_ACCESS_METHOD(METHOD) \
+(instancetype) METHOD {\
    static id s;\
    static dispatch_once_t predicate;\
    dispatch_once(&predicate, ^{\
        s = [[self alloc] init];\
    });\
    return s;\
}

#define flipCoords(location, height) location.y = height - location.y;

// https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Linear_B.C3.A9zier_curves
#define linearCount(__start__,__end__, __division_factor__) MAX(CGFloatCeil(CGPointDist(__start__, __end__) / __division_factor__), 1)
#define linear(__start__, __end__, __what__, __step__) __start__.__what__ + (__end__.__what__ - __start__.__what__) * __step__
// https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Quadratic_B.C3.A9zier_curves
#define quadCount(__start__,__control__,__end__,__division_factor__) \
                    MAX(CGFloatCeil((\
                        /* approximate length of the quadratic curve */\
                        CGPointDist(__start__, __control__) +\
                        CGPointDist(__control__, __end__)\
                        ) / __division_factor__), 1)
#define quad(__start__,__control__,__end__,__what__,__step__) square(1 - __step__) * __start__.__what__ + 2 * (1 - __step__) * __step__ * __control__.__what__ + square(t) * __end__.__what__

// ~--- RBBitmap: a row-major on-heap array.
//
typedef uint8_t * RBBitmap;
typedef uint8_t const* RBConstantBitmap;

RB_INLINE RBBitmap
RBBitmapCreate(const NSUInteger numBits) {
    NSLog(@"[RBBitmap] creating %ld bytes", (long)((numBits + 7) / 8));
    return calloc((numBits + 7) / 8, sizeof(uint8_t));
}
RB_INLINE BOOL
RBBitmapGet(const RBConstantBitmap bitmap, const NSUInteger width, const NSUInteger height, const NSUInteger x, const NSUInteger y) {
    NSUInteger idxBits = y * width + x;
    if (idxBits > width * height) {
        return NO;
    }
    NSUInteger idxBytes = idxBits >> 3; // idxBits / 8
    NSUInteger shift = idxBits & 7;     // idxBits % 8
    return bitmap[idxBytes] & (1 << shift);
}
/*!
 * @return No if out of bounds, yes otherwise
 */
RB_INLINE BOOL
RBBitmapSet(const RBBitmap bitmap, const NSUInteger width, const NSUInteger height, const NSUInteger x, const NSUInteger y) {
    NSUInteger idxBits = y * width + x;
    if (idxBits > width * height) {
        return NO;
    }
    NSUInteger idxBytes = idxBits >> 3; // idxBits / 8
    NSUInteger shift = idxBits & 7;     // idxBits % 8
    bitmap[idxBytes] |= 1 << shift;
    return YES;
}

/*!
 * @return No if out of bounds, yes otherwise
 */
RB_INLINE BOOL
RBBitmapUnset(const RBBitmap bitmap, const NSUInteger width, const NSUInteger height, const NSUInteger x, const NSUInteger y) {
    NSUInteger idxBits = y * width + x;
    if (idxBits > width * height) {
        return NO;
    }
    NSUInteger idxBytes = idxBits >> 3; // idxBits / 8
    NSUInteger shift = idxBits & 7;     // idxBits % 8
    bitmap[idxBytes] &= ~(1 << shift);
    return YES;
}


#if DEBUG
#define MAIN_THREAD_CHECK() if(![NSThread isMainThread]) \
    @throw [NSException exceptionWithName:@"NotOnMainThreadException" \
                                   reason:@"method not called on main thread"\
                                  userInfo:nil];
#else
#define MAIN_THREAD_CHECK()
#endif

static uint64_t __start_time, __check_time;
#define swBegin() __check_time = __start_time = mach_absolute_time();
#define swCheck(info) {\
    uint64_t __current_time = mach_absolute_time();\
    NSLog(@"[TIME] %@ %lld, total = %lld", info, __current_time - __check_time, __current_time - __start_time);\
    __check_time = __current_time;\
}

RB_INLINE void
RBToast(const NSString *msg) {
    [[NSNotificationCenter defaultCenter] \
     postNotificationName:RBNotificationToastName
        object:nil
        userInfo:@{RBNotificationToastMessageKey:msg}];
}

typedef enum RBError {
    kRBErrorCannotLoadUserDefaults = 1,
    kRBErrorCannotSaveUserDefaults,
    kRBErrorEmptyCanvasId,
} RBError;

typedef enum RBState {
    kRBStateInitializing = 0,
    kRBStateSuccess = 1,
    kRBStateError = -1,
} RBState;

typedef enum RBBezierPathAction {
    kRBBezierPathLine,
    kRBBezierPathQuad
} RBBezierPathAction;

typedef enum RBStrokeType {
    kRBEraser = NO,
    kRBBrush = YES,
} RBStrokeType;

typedef struct RBBezierSegment {
    RBBezierPathAction action;
    CGPoint control;
    CGPoint end;
} RBBezierSegment;

typedef void (^RBLineDrawerType)(CGPoint, CGPoint);
typedef void (^RBQuadDrawerType)(CGPoint, CGPoint, CGPoint);

typedef NSInteger RBCoordInt;

/*!
 * A pointer to a RBBezierSegment that does not allow changing contents.
 */
typedef RBBezierSegment const * RBBezierSegmentRef;

RB_INLINE RBBezierSegment
RBBezierSegmentMake(const RBBezierPathAction action, const CGPoint control, const CGPoint end) {
    RBBezierSegment seg;
    seg.action = action;
    seg.control = control;
    seg.end = end;
    return seg;
}

RB_INLINE
NSString *parseShareURL(const NSURL *url) {
    NSString *canvasId;
    do {
        if((canvasId = [url host]).length)
            break;
        
        // http://stackoverflow.com/a/5410443
        for(NSString *keyValuePairString in [[url query] componentsSeparatedByString:@"&"])
        {
            NSArray *keyValuePairArray = [keyValuePairString componentsSeparatedByString:@"="];
            if ([keyValuePairArray count] < 2) continue; // Verify that there is at least one key, and at least one value.  Ignore extra = signs
            NSString *key = [[keyValuePairArray objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString *value = [[keyValuePairArray objectAtIndex:1]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if (!value.length)
                continue; // the for loop;
            if ([key caseInsensitiveCompare:@"id"] == NSOrderedSame) {
                canvasId = value;
                break;
            }
        }
        //        if(canvasId.length)
        //            break;
        // if need to add more interpolation, uncomment the above two lines
    } while (NO);
    return canvasId.length ? canvasId : nil;
}

#define RBHost @"http://remoteboard.github.io"
#define RBHelpPage RBHost "/help.html"

RB_INLINE
NSURL *constructURL(const NSString *canvasId) {
    if(!canvasId.length)
        return nil;
    return [NSURL URLWithString:[NSString stringWithFormat:RBHost "/index.html?id=%@",canvasId,nil]];
}



//RB_INLINE
//CGContextRef RBBitmapContextCreate (const CGSize size)
//{
//    // http://stackoverflow.com/a/10870188
//    
//    float scaleFactor = [[UIScreen mainScreen] scale];
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    
//    CGContextRef context = CGBitmapContextCreate(NULL,
//                                                 size.width * scaleFactor,
//                                                 size.height * scaleFactor,
//                                                 8,
//                                                 size.width * scaleFactor * 4,
//                                                 colorSpace,
//                                                 kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Host);
//    if(!context) {
//        NSLog(@"[RBBitmapContextCreate] Cannot allocate data");
//        return NULL;
//    }
//    
//    CGContextScaleCTM(context, scaleFactor, scaleFactor);
//    CGContextConcatCTM(context, CGAffineTransformMake(1, 0, 0, -1, 0, size.height));
//    return context;
//}

#endif
