//
//  RBStroke.m
//  drawingapp
//
//  Created by Elsk Hong on 7/5/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import "RBStroke.h"

@implementation RBStroke

- (instancetype) init {
    NSLog(@"[RBStroke] FATAL: should not call RBStroke with init or new method.\n\tUse initWithPoint: or strokeWithPoint: instead.");
    CGPoint p = {.x = 0, .y = 0};
    return [self initWithPoint:p];
}

+ (instancetype) strokeFromPoints:(id)points withId:(NSString *)strokeId {
    if(!points || ![points isKindOfClass:[NSArray class]]) {
        NSLog(@"[RBStroke] FATAL: cannot deserialize from %@", points);
        return nil;
    }
    BOOL __block ok = YES;
    RBStroke __block *stroke = nil;
    [points enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        do {
            if(![obj isKindOfClass:[NSArray class]])
                break;
            NSArray *array = obj;
            if(array.count != 2)
                break;
            id x = [array objectAtIndex:0], y = [array objectAtIndex:1];
            if(![x isKindOfClass:[NSNumber class]] || ![y isKindOfClass:[NSNumber class]])
                break;
            CGPoint point = CGPointMakeFromNSNumbers(x,y);
            if(idx == 0) {
                stroke = [RBStroke strokeWithPoint:point];
            } else {
                [stroke addPoint:point];
            }
            return; // done, handle next point
        } while(NO);
        // error
        *stop = YES;
        ok = NO;
    }];
    if(!ok) {
        NSLog(@"[RBStroke] FATAL: cannot deserialize from %@", points);
        return nil;
    }
    if(!stroke) {
        NSLog(@"[RBStroke] FATAL: array %@ is empty", points);
        return nil;
    }
    [stroke finish];
    [stroke setStrokeId:strokeId];
    return stroke;

}

+ (instancetype) strokeWithPoint:(CGPoint)startPoint {
    return [[self alloc] initWithPoint:startPoint];
}

- (instancetype) initWithPoint:(CGPoint)startPoint {
    self = [super init];
    if(self) {
        _mutex = [NSObject new];
        _points = [NSMutableArray new];
//        _path = [UIBezierPath bezierPath];
//        [_path moveToPoint:startPoint];
        _bezierPath = [RBBezierPath bezierPathWithStartPoint:startPoint];
        [_points addObject:WRAP_POINT(startPoint)];
    }
//    NSLog(@"[RBStroke] %@ creating (%f, %f)", self, startPoint.x, startPoint.y);
    return self;
}
- (void) addPoint:(CGPoint)newPoint {
    [self addPoint:newPoint returnStartPoint:NULL controlPoint:NULL endPoint:NULL];
}

// first segment is slightly thiner than others. That is totally fine; it
// makes it look like a real whiteboard!

- (BOOL) addPoint:(CGPoint)newPoint
 returnStartPoint:(CGPoint *)startPointPtr
     controlPoint:(CGPoint *)controlPointPtr
         endPoint:(CGPoint *)endPointPtr {
    //    NSLog(@"[RBStroke] %@ adding (%f, %f)", self, newPoint.x, newPoint.y);
    @synchronized (_mutex) {
        if(_finished)
            @throw [NSException exceptionWithName:@"Finished" reason:@"Stroke is finished" userInfo:@{}];
        CGPoint lastPoint = UNWRAP_POINT([_points objectAtIndex:_points.count - 1]);
#warning TODO Threashold should be in UIKit units, not in application-wise units. This is especially important after scaling is implemented.
        if(CGFloatAbs(lastPoint.x - newPoint.x) + CGFloatAbs(lastPoint.y - newPoint.y) < RBDrawing_DISTANCE_THRESHOLD)
            return NO;
        CGPoint c = CGPointMake((lastPoint.x + newPoint.x) / 2., (lastPoint.y + newPoint.y) / 2.);
        if(startPointPtr) *startPointPtr = [_bezierPath currentPoint];
        [_bezierPath quadTo:c controlPoint:lastPoint];
//        [_path addQuadCurveToPoint:c controlPoint:lastPoint];
        [_points addObject:WRAP_POINT(newPoint)];
        if(controlPointPtr) *controlPointPtr = lastPoint;
        if(endPointPtr) *endPointPtr = c;
        return YES;
    }
}
- (void) finish {
    [self finishAndReturnStartPoint:NULL endPoint:NULL createBitmap:YES];
}

- (BOOL) finishAndReturnStartPoint:(CGPoint *)startPointPtr
                          endPoint:(CGPoint *)endPointPtr
                      createBitmap:(BOOL)createBitmap {
    @synchronized (_mutex) {
        if(_finished)
            return NO;
        _finished = true;
        NSUInteger size = _points.count;
        CGPoint lastPoint = UNWRAP_POINT([_points objectAtIndex:size - 1]);
        if(startPointPtr) *startPointPtr = [_bezierPath currentPoint];
        if(size > 2)
            [_bezierPath lineTo:lastPoint];
        if(endPointPtr) *endPointPtr = lastPoint;
        if(createBitmap)
            [_bezierPath createBitmap];
        return YES;
    }
}

#define RBStrokeCheckFinished(b) \
    if(!b) \
        @throw [NSException exceptionWithName:@"IllegalStateException" reason:@"stroke is not finished" userInfo:nil];

- (NSArray *) pointsArray {
    @synchronized(self) {
        RBStrokeCheckFinished(_finished);
        return [NSArray arrayWithArray:_points];
    }
}

- (BOOL) intersectWithCircleAt:(CGPoint)center radius:(CGFloat)radius {
    @synchronized(self){
        RBStrokeCheckFinished(_finished);
        return [_bezierPath intersectWithCircleAt:center radius:radius];
    }
}


- (void) drawPathUsingLineDrawer:(void (^)(CGPoint start, CGPoint end))lineDrawer
                      quadDrawer:(void (^)(CGPoint start, CGPoint control, CGPoint end))quadDrawer {
    @synchronized(self){
        [_bezierPath drawPathUsingLineDrawer:lineDrawer quadDrawer:quadDrawer];
    }
}

- (void) setStrokeId:(NSString *)strokeId {
    @synchronized(self){
        RBStrokeCheckFinished(_finished);
        if(_strokeId) {
#if DEBUG
            @throw [NSException exceptionWithName:@"IllegalStateException"
                                           reason:@"setStrokeId is called multiple times"
                                         userInfo:@{@"stroke":self}
                    ];
#else
            NSLog(@"[RBStroke] FATAL: setStrokeId is called multiple times!");
            return;
#endif
        }
        _strokeId = strokeId;
    }
}

@end
