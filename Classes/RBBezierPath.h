//
//  RBBezierPath.h
//  drawingapp
//
//  Created by Elsk Hong on 7/14/15.
//
//

#import <Foundation/Foundation.h>
#import "RBCommon.h"

#define RBBezierPathSegmentsInitialSize 4

#define RBBezierSegmentQuadMake(control, end) RBBezierSegmentMake(kRBBezierPathQuad, control, end);
#define RBBezierSegmentLineMake(end) RBBezierSegmentMake(kRBBezierPathLine, CGPointMake(0 ,0), end);

/*!
 * A non-concurrent/synchronized array list of RBBezierSegments. Extra care
 * should be taken of if used in several threads.
 */
@interface RBBezierPath : NSObject {
    RBBezierSegment *_segments;
    NSUInteger _capacity, _size;
    // Comparisons between NSUInteger and NSInteger is crazy. So I will just use NSInteger here.
    RBCoordInt _xMin, _xMax, _yMin, _yMax;
    RBBitmap _bitmap;
    /*! # bits allocated in bitmap */
    NSUInteger _bitmapSize, _bitmapWidth, _bitmapHeight;
}
@property (readonly, atomic) CGPoint currentPoint;
@property (readonly, atomic) CGPoint startPoint;

+ (instancetype) bezierPathWithStartPoint:(CGPoint)start;
- (void) lineTo:(CGPoint)end;
- (void) quadTo:(CGPoint)end controlPoint:(CGPoint)control;
- (void) enumerateSegmentsUsingBlock:(void (^)(RBBezierSegmentRef, NSUInteger, BOOL *))block;
- (void) drawPathUsingLineDrawer:(void (^)(CGPoint start, CGPoint end))lineDrawer
                      quadDrawer:(void (^)(CGPoint start, CGPoint control, CGPoint end))quadDrawer;
- (BOOL) intersectWithCircleAt:(CGPoint)center radius:(CGFloat)radius;

- (void) createBitmap;

@end
