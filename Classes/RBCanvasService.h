//
//  RBCanvasService.h
//  drawingapp
//
//  Created by Elsk Hong on 7/6/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBStroke.h"
#import "PaintingViewController.h"
#import "RBCanvasProfile.h"

/*!
 * The CONTROLLER of the MVC framework. It
 * has a reference to the view (viewController) and
 * a reference to the model (currentProfile).
 * This is a singleton because there is only one controller
 * in the whole app.
 */
@interface RBCanvasService : NSObject {
    RBCanvasProfile *_currentProfile;
    /*! Whenever need to access _currentProfile, gain a monitor lock first*/
    id _currentProfileMutex;
    PaintingViewController __weak *_viewController;
}

// ~--- getters and setters
+ (instancetype) instance;
- (NSString *) currentCanvasId;
- (void) setViewController:(PaintingViewController *)viewController;

// ~--- VIEW UI interactions -> change MODEL

/*!
 * Must be called on main thread.
 * Create a new RBCanvasProfile and replace it with the old one.
 * postcondition: _currentProfile.canvasId = canvasId,
 * _currentProfile.state = kRBStateInitializing
 */
- (void) openCanvas:(NSString *)canvasId;
/*!
 * Add stroke to _currentProfile.
 */
- (void) addStroke:(RBStroke *)stroke;
/*!
 * Remove stroke from _currentProfile. (called by UI -> changes MODEL)
 */
- (void) asyncRemoveAll:(NSArray * /*of RBStroke*/)array;
/*!
 * Called by VIEW to do certain operations on MODEL. This method
 * DOES NOT change VIEW.
 */
- (void) onEraserMovedTo:(CGPoint)center
                       radius:(CGFloat)radius
         addIntersectionToSet:(NSMutableSet* /*of RBStroke*/)set;
- (void) onEraserAlongQuadraticCurveWithStartPoint:(CGPoint)start
                                      controlPoint:(CGPoint)control
                                          endPoint:(CGPoint)end
                                            radius:(CGFloat)radius
                              addIntersectionToSet:(NSMutableSet* /*of RBStroke*/)set;
/*!
 * @return a localized message for toast
 */
- (NSString *) toggleFavorite;

/*! 
 * Convenience method to asynchronously redraw the current profile (on main thread).
 * This method can be called on any thread.
 */
- (void) redrawCurrentProfile;

/*!
 * On receiving push notification from server, notify profile to pull
 * data. (Notification -> MODEL change -> VIEW change)
 */
- (void) asyncDownloadAndDrawStrokesTill:(NSTimeInterval)tillTime;
/*!
 * On receiving push notification from server, notify profile to delete
 * strokes.
 */
- (void) onServerRemovedStrokeWithId:(NSString *)strokeId;


// ~--- methods called by RBCanvasProfile; MODEL changed -> change VIEW

- (void) onProfileShouldRedraw:(RBCanvasProfile *)profile;
- (void) onProfile:(RBCanvasProfile *)profile
                 addedContent:(NSArray *)strokes
                   error:(NSError *)error
    shouldToastIfSuccess:(BOOL)shouldToastIfSuccess;
- (void) onProfile:(RBCanvasProfile *)profile addedStrokeWithId:(NSString *)strokeId;
- (void) onProfile:(RBCanvasProfile *)profile cannotAddStrokeWithError:(NSError*)error;
- (void) onProfile:(RBCanvasProfile *)profile deletedStrokeWithId:(NSString *)strokeId;
- (void) onProfile:(RBCanvasProfile *)profile cannotDeleteStroke:(RBStroke *)stroke error:(NSError *)error;



@end
