/*
     File: PaintingView.m
 Abstract: The class responsible for the finger painting. The class wraps the 
 CAEAGLLayer from CoreAnimation into a convenient UIView subclass. The view 
 content is basically an EAGL surface you render your OpenGL scene into.
  Version: 1.13
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 
*/

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#import <GLKit/GLKit.h>

#import "PaintingView.h"
#import "RBCanvasService.h"
#import "shaderUtil.h"
#import "fileUtil.h"
#import "debug.h"

//CONSTANTS:

#define kBrushOpacity		.7
#define kBrushPixelStep		1.5
#define kBrushScale			10
#define kEraserScale		2

#define kRBBackgroundA        1

// blackbord
//#define kRBBackgroundR        0.  / 255.
//#define kRBBackgroundG        48. / 255.
//#define kRBBackgroundB        42. / 255.
// grayboard
#define kRBBackgroundR        (0xcc) / 255.
#define kRBBackgroundG        (0xcc) / 255.
#define kRBBackgroundB        (0xcc) / 255.

// whiteboard
//#define kRBBackgroundR        1.
//#define kRBBackgroundG        1.
//#define kRBBackgroundB        1.

// this is texture dependent; when texture changes this value must also change.
#define kEraserTextureHaloFactor    .5

// This four macros requires a "height" in context.
// Arguments are in Application-wise units
#define RBLineDrawer ^(CGPoint start, CGPoint end) {\
    drawLine(CGPointScale(start, _scaleFactor),\
        CGPointScale(end, _scaleFactor),\
        height, self.contentScaleFactor, vboId);\
}
#define RBQuadDrawer ^(CGPoint start, CGPoint control, CGPoint end) {\
    drawQuad(CGPointScale(start, _scaleFactor),\
        CGPointScale(control, _scaleFactor),\
        CGPointScale(end, _scaleFactor),\
        height, self.contentScaleFactor, vboId);\
}
#define RBLineRenderer(__ld__) ^(CGPoint start, CGPoint end) {\
    prepareRender(context, viewFramebuffer);\
    __ld__(start, end);\
    displayBuffer(context, viewRenderbuffer);\
}
#define RBQuadRenderer(__qd__) ^(CGPoint start, CGPoint control, CGPoint end) {\
    prepareRender(context, viewFramebuffer);\
    __qd__(start, control, end);\
    displayBuffer(context, viewRenderbuffer);\
}

#define RBUseBrush() \
    glBindTexture(GL_TEXTURE_2D, brushTexture.id);\
    glUniform1f(program[PROGRAM_POINT].uniform[UNIFORM_POINT_SIZE], brushTexture.width / kBrushScale * self.contentScaleFactor * _scaleFactor);\
    currentStrokeType = kRBBrush;\

#define RBUseEraser() \
    glBindTexture(GL_TEXTURE_2D, eraserTexture.id);\
    glUniform1f(program[PROGRAM_POINT].uniform[UNIFORM_POINT_SIZE], eraserTexture.width / kEraserScale * self.contentScaleFactor * _scaleFactor);\
    currentStrokeType = kRBEraser;\

#define RBUseStrokeType(__type__)\
    if(currentStrokeType != __type__) {\
        switch(__type__) {\
            case kRBBrush:\
                RBUseBrush(); break;\
            case kRBEraser:\
                RBUseEraser(); break;\
        }\
    }

// Shaders
enum {
    PROGRAM_POINT,
    NUM_PROGRAMS
};

enum {
	UNIFORM_MVP,
    UNIFORM_POINT_SIZE,
    UNIFORM_VERTEX_COLOR,
    UNIFORM_TEXTURE,
	NUM_UNIFORMS
};

enum {
	ATTRIB_VERTEX,
	NUM_ATTRIBS
};

typedef struct {
	char *vert, *frag;
	GLint uniform[NUM_UNIFORMS];
	GLuint id;
} programInfo_t;

programInfo_t program[NUM_PROGRAMS] = {
    { "point.vsh",   "point.fsh" },     // PROGRAM_POINT
};


// Texture
typedef struct {
    GLuint id;
    GLsizei width, height;
} textureInfo_t;

@interface RBImage : UIImage
@property (readwrite, strong) dispatch_block_t deallocBlock;
@end

@implementation RBImage

- (void)dealloc {
    if(self.deallocBlock) {
        self.deallocBlock();
    }
}

@end

@interface PaintingView()
{
	// The pixel dimensions of the backbuffer
	GLint backingWidth;
	GLint backingHeight;
	
	EAGLContext *context;
	
	// OpenGL names for the renderbuffer and framebuffers used to render to this view
	GLuint viewRenderbuffer, viewFramebuffer;
    
    // OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist)
    GLuint depthRenderbuffer;
	
    textureInfo_t brushTexture;     // brush texture
    textureInfo_t eraserTexture;    // brush texture
    GLfloat brushColor[4];          // brush color
    
//	Boolean	firstTouch;
	Boolean needsErase;
    
    // Shader objects
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;
    
    // Buffer Objects
    GLuint vboId;
    
    BOOL initialized;
    RBStrokeType currentStrokeType;
    NSMutableSet *ongoingTouches;
    
    /*! Application logical units * _scaleFactor = UIKit units. */
    CGFloat _scaleFactor;
    /*! UIKit width * _inverseScaleFactor = Application logical width. */
    CGFloat _inverseScaleFactor;
}

@end

@implementation PaintingView

//@synthesize  location;
//@synthesize  previousLocation;

// Implement this to override the default layer class (which is [CALayer class]).
// We do this so that our view will be backed by a layer that is capable of OpenGL ES rendering.
+ (Class)layerClass
{
	return [CAEAGLLayer class];
}

- (instancetype)init
{
    if ((self = [super init])) {
        if(![self doInit])
            return nil;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        if(![self doInit])
            return nil;
    }
    return self;
}

// The GL view is stored in the nib file. When it's unarchived it's sent -initWithCoder:
- (instancetype)initWithCoder:(NSCoder*)coder {
    if ((self = [super initWithCoder:coder])) {
        if(![self doInit])
            return nil;
	}
	return self;
}

- (BOOL) doInit {
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
    
    eaglLayer.opaque = YES;
    // In this application, we want to retain the EAGLDrawable contents after a call to presentRenderbuffer.
    eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:YES], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!context || ![EAGLContext setCurrentContext:context]) {
        return NO;
    }
    
    // Set the view's scale factor as you wish
    self.contentScaleFactor = [[UIScreen mainScreen] scale];
    
    // Make sure to start with a cleared buffer
    needsErase = YES;
    ongoingTouches = [NSMutableSet setWithCapacity:4];
    return YES;
}

// If our view is resized, we'll be asked to layout subviews.
// This is the perfect opportunity to also update the framebuffer so that it is
// the same size as our display area.
-(void)layoutSubviews
{
    [self setupFrame];
    [super layoutSubviews];
	[EAGLContext setCurrentContext:context];
    
    if (!initialized) {
        initialized = [self initGL];
    }
    else {
        [self resizeFromLayer:(CAEAGLLayer*)self.layer];
    }
	
	// Clear the framebuffer the first time it is allocated
	if (needsErase) {
		[self erase];
		needsErase = NO;
	}
}

// programatically center the view, keeping its ratio and maximizing its size.
-(void)setupFrame {
    CGSize realSize = self.frame.size;
    CGFloat actualRatio = realSize.width / realSize.height;
    if(CGFloatAbs(actualRatio - RBBoardWHRatio) < 1e-3) {
        _scaleFactor = realSize.width / RBBoardWidth;
    } else if (actualRatio > RBBoardWHRatio) { // too wide
        _scaleFactor = realSize.height / RBBoardHeight;
//        CGFloat wantedWidth = realSize.height * RBBoardWHRatio;
//        self.frame = CGRectMake((realSize.width - wantedWidth) / 2, 0,
//                                wantedWidth, realSize.height);
    } else { // too high
        _scaleFactor = realSize.width / RBBoardWidth;
//        CGFloat wantedHeight = realSize.width / RBBoardWHRatio;
//        self.frame = CGRectMake(0, (realSize.height - wantedHeight) / 2,
//                                realSize.width, wantedHeight);
    }
    _inverseScaleFactor = 1/_scaleFactor;
}

- (void)setupShaders
{
	for (int i = 0; i < NUM_PROGRAMS; i++)
	{
		char *vsrc = readFile(pathForResource(program[i].vert));
		char *fsrc = readFile(pathForResource(program[i].frag));
		GLsizei attribCt = 0;
		GLchar *attribUsed[NUM_ATTRIBS];
		GLint attrib[NUM_ATTRIBS];
		GLchar *attribName[NUM_ATTRIBS] = {
			"inVertex",
		};
		const GLchar *uniformName[NUM_UNIFORMS] = {
			"MVP", "pointSize", "vertexColor", "texture",
		};
		
		// auto-assign known attribs
		for (int j = 0; j < NUM_ATTRIBS; j++)
		{
			if (strstr(vsrc, attribName[j]))
			{
				attrib[attribCt] = j;
				attribUsed[attribCt++] = attribName[j];
			}
		}
		
		glueCreateProgram(vsrc, fsrc,
                          attribCt, (const GLchar **)&attribUsed[0], attrib,
                          NUM_UNIFORMS, &uniformName[0], program[i].uniform,
                          &program[i].id);
		free(vsrc);
		free(fsrc);
        
        // Set constant/initalize uniforms
        if (i == PROGRAM_POINT)
        {
            glUseProgram(program[PROGRAM_POINT].id);
            
            // the brush texture will be bound to texture unit 0
            glUniform1i(program[PROGRAM_POINT].uniform[UNIFORM_TEXTURE], 0);
            
            // viewing matrices
            GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(0, backingWidth, 0, backingHeight, -1, 1);
            GLKMatrix4 modelViewMatrix = GLKMatrix4Identity; // this sample uses a constant identity modelView matrix
            GLKMatrix4 MVPMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
            
            glUniformMatrix4fv(program[PROGRAM_POINT].uniform[UNIFORM_MVP], 1, GL_FALSE, MVPMatrix.m);
        
            // point size
            // no need. this is set in UseBrush and UseEraser
//            glUniform1f(program[PROGRAM_POINT].uniform[UNIFORM_POINT_SIZE], brushTexture.width / kBrushScale * self.contentScaleFactor * _scaleFactor);
            
            // initialize brush color
            glUniform4fv(program[PROGRAM_POINT].uniform[UNIFORM_VERTEX_COLOR], 1, brushColor);
        }
	}
    
    glError();
}

// Create a texture from an image
- (textureInfo_t)textureFromName:(NSString *)name
{
    CGImageRef		brushImage;
	CGContextRef	brushContext;
	GLubyte			*brushData;
	size_t			width, height;
    GLuint          texId;
    textureInfo_t   texture;
    
    // First create a UIImage object from the data in a image file, and then extract the Core Graphics image
    brushImage = [UIImage imageNamed:name].CGImage;
    
    // Get the width and height of the image
    width = CGImageGetWidth(brushImage);
    height = CGImageGetHeight(brushImage);
    
    // Make sure the image exists
    if(brushImage) {
        // Allocate  memory needed for the bitmap context
        brushData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
        // Use  the bitmatp creation function provided by the Core Graphics framework.
        brushContext = CGBitmapContextCreate(brushData, width, height, 8, width * 4, CGImageGetColorSpace(brushImage), kCGImageAlphaPremultipliedLast);
        // After you create the context, you can draw the  image to the context.
        CGContextDrawImage(brushContext, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), brushImage);
        // You don't need the context at this point, so you need to release it to avoid memory leaks.
        CGContextRelease(brushContext);
        // Use OpenGL ES to generate a name for the texture.
        glGenTextures(1, &texId);
        // Bind the texture name.
        glBindTexture(GL_TEXTURE_2D, texId);
        // Set the texture parameters to use a minifying filter and a linear filer (weighted average)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Specify a 2D texture image, providing the a pointer to the image data in memory
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)width, (int)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, brushData);
        // Release  the image data; it's no longer needed
        free(brushData);
        
        texture.id = texId;
        texture.width = (int)width;
        texture.height = (int)height;
    }
    
    return texture;
}

// Create a texture from an image
- (void)textures:(textureInfo_t *)textures fromNames:(NSArray *)names
{
    CGImageRef		brushImage;
    CGContextRef	brushContext;
    GLubyte			*brushData;
    size_t			width, height;
    GLuint          texIds[names.count];
    
    // Use OpenGL ES to generate names for the textures.
    glGenTextures((int)names.count, texIds);
    
    for(NSUInteger i = 0, len = names.count; i < len; i++) {
    
        NSString *name = [names objectAtIndex:i];
        
        // First create a UIImage object from the data in a image file, and then extract the Core Graphics image
        brushImage = [UIImage imageNamed:name].CGImage;
        
        // Get the width and height of the image
        width = CGImageGetWidth(brushImage);
        height = CGImageGetHeight(brushImage);
        
        // Make sure the image exists
        if(brushImage) {
            // Allocate  memory needed for the bitmap context
            brushData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
            // Use  the bitmatp creation function provided by the Core Graphics framework.
            brushContext = CGBitmapContextCreate(brushData, width, height, 8, width * 4, CGImageGetColorSpace(brushImage), kCGImageAlphaPremultipliedLast);
            // After you create the context, you can draw the  image to the context.
            CGContextDrawImage(brushContext, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), brushImage);
            // You don't need the context at this point, so you need to release it to avoid memory leaks.
            CGContextRelease(brushContext);
            // Bind the texture name.
            glBindTexture(GL_TEXTURE_2D, texIds[i]);
            // Set the texture parameters to use a minifying filter and a linear filer (weighted average)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            // Specify a 2D texture image, providing the a pointer to the image data in memory
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)width, (int)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, brushData);
            // Release  the image data; it's no longer needed
            free(brushData);
            
            textures[i].id = texIds[i];
            textures[i].width = (int)width;
            textures[i].height = (int)height;
        }
    }
    
}

- (BOOL)initGL
{
    textureInfo_t textures[2];
    
    // Generate IDs for a framebuffer object and a color renderbuffer
	glGenFramebuffers(1, &viewFramebuffer);
	glGenRenderbuffers(1, &viewRenderbuffer);
	
	glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
	// This call associates the storage for the current render buffer with the EAGLDrawable (our CAEAGLLayer)
	// allowing us to draw into a buffer that will later be rendered to screen wherever the layer is (which corresponds with our view).
	[context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(id<EAGLDrawable>)self.layer];
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, viewRenderbuffer);
	
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
	
	// For this sample, we do not need a depth buffer. If you do, this is how you can create one and attach it to the framebuffer:
//    glGenRenderbuffers(1, &depthRenderbuffer);
//    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
//    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		return NO;
	}
    
    // Setup the view port in Pixels
    glViewport(0, 0, backingWidth, backingHeight);
    
    // Create a Vertex Buffer Object to hold our data
    glGenBuffers(1, &vboId);
    
    // Load the brush texture
    // The reason That I have two textures is that
    // if I want to replace the eraser texture it would be convenient.
    [self textures:textures fromNames:@[@"Eraser.png", @"Particle.png"]];
    eraserTexture = textures[0];
    brushTexture = textures[1];
    RBUseBrush();
    
    // Load shaders
    [self setupShaders];
    
    // Enable blending and set a blending function appropriate for premultiplied alpha pixel data
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    return YES;
}


- (BOOL)resizeFromLayer:(CAEAGLLayer *)layer
{
	// Allocate color buffer backing based on the current layer size
    glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:layer];
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
	
    // For this sample, we do not need a depth buffer. If you do, this is how you can allocate depth buffer backing:
//    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
//    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
	
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
        NSLog(@"Failed to make complete framebuffer objectz %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        return NO;
    }
    
    // Update projection matrix
    GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(0, backingWidth, 0, backingHeight, -1, 1);
    GLKMatrix4 modelViewMatrix = GLKMatrix4Identity; // this sample uses a constant identity modelView matrix
    GLKMatrix4 MVPMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
    
    glUseProgram(program[PROGRAM_POINT].id);
    glUniformMatrix4fv(program[PROGRAM_POINT].uniform[UNIFORM_MVP], 1, GL_FALSE, MVPMatrix.m);
    
    // Update viewport
    glViewport(0, 0, backingWidth, backingHeight);
	
    return YES;
}

// Releases resources when they are not longer needed.
- (void)dealloc
{
    // Destroy framebuffers and renderbuffers
	if (viewFramebuffer) {
        glDeleteFramebuffers(1, &viewFramebuffer);
        viewFramebuffer = 0;
    }
    if (viewRenderbuffer) {
        glDeleteRenderbuffers(1, &viewRenderbuffer);
        viewRenderbuffer = 0;
    }
	if (depthRenderbuffer)
	{
		glDeleteRenderbuffers(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}
    // texture
    if (brushTexture.id) {
		glDeleteTextures(1, &brushTexture.id);
		brushTexture.id = 0;
	}
    if (eraserTexture.id) {
        glDeleteTextures(1, &eraserTexture.id);
        eraserTexture.id = 0;
    }
    // vbo
    if (vboId) {
        glDeleteBuffers(1, &vboId);
        vboId = 0;
    }
    
    // tear down context
	if ([EAGLContext currentContext] == context)
        [EAGLContext setCurrentContext:nil];
}

// Erases the screen
- (void)erase
{
    prepareRender(context, viewFramebuffer);
    [self clearBuffer];
	// Display the buffer
    displayBuffer(context, viewRenderbuffer);
}

- (void)clearBuffer
{
    // Clear the buffer
    glClearColor(kRBBackgroundR,kRBBackgroundG,kRBBackgroundB,kRBBackgroundA);
    glClear(GL_COLOR_BUFFER_BIT);
}

static GLfloat*		vertexBuffer = NULL;
static NSUInteger	vertexMax = 64;

RB_INLINE void
prepareRender(EAGLContext *context, GLuint viewFramebuffer) {
    [EAGLContext setCurrentContext:context];
    glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
}

RB_INLINE void
drawVBO(NSUInteger vertexCount, GLuint vboId) {
    // Load data to the Vertex Buffer Object
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glBufferData(GL_ARRAY_BUFFER, vertexCount*2*sizeof(GLfloat), vertexBuffer, GL_DYNAMIC_DRAW);
    
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, 0);
    
    //Draw
    glUseProgram(program[PROGRAM_POINT].id);
    glDrawArrays(GL_POINTS, 0, (int)vertexCount);
}
RB_INLINE void
displayBuffer(EAGLContext *context, GLuint viewRenderbuffer) {
    // Display the buffer
    glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

void
drawLine(CGPoint start, CGPoint end, CGFloat height, CGFloat scale, GLuint vboId) {
    NSUInteger			vertexCount = 0,
    count,
    i;
    GLfloat             t;
    // Convert locations from Points to Pixels
    flipCoords(start, height);
    flipCoords(end, height);
    start.x *= scale;
    start.y *= scale;
    end.x *= scale;
    end.y *= scale;
    
    // Allocate vertex array buffer
    if(vertexBuffer == NULL)
        vertexBuffer = malloc(vertexMax * 2 * sizeof(GLfloat));
    
    // Add points to the buffer so there are drawing points every X pixels
    count = linearCount(start, end, kBrushPixelStep);
    for(i = 0; i < count; ++i) {
        if(vertexCount == vertexMax) {
            vertexMax = 2 * vertexMax;
            vertexBuffer = realloc(vertexBuffer, vertexMax * 2 * sizeof(GLfloat));
        }
        t = ((GLfloat)i) / ((GLfloat)count);
        vertexBuffer[2 * vertexCount + 0] = linear(start, end, x, t);
        vertexBuffer[2 * vertexCount + 1] = linear(start, end, y, t);
        vertexCount += 1;
    }
    
    drawVBO(vertexCount, vboId);
}

void
drawQuad(CGPoint start, CGPoint control, CGPoint end, CGFloat height, CGFloat scale, GLuint vboId) {
    NSUInteger			vertexCount = 0,
    count,
    i;
    GLfloat             t;
    // Convert locations from Points to Pixels
    flipCoords(start, height);
    flipCoords(control, height);
    flipCoords(end, height);
    start.x *= scale;
    start.y *= scale;
    end.x *= scale;
    end.y *= scale;
    control.x *= scale;
    control.y *= scale;
    
    // Allocate vertex array buffer
    if(vertexBuffer == NULL)
        vertexBuffer = malloc(vertexMax * 2 * sizeof(GLfloat));
    
    // Add points to the buffer so there are drawing points every X pixels
    count = quadCount(start, control, end, kBrushPixelStep);
    if (count == 1) { // easy case. just 1 point!
        vertexBuffer[0] = start.x;
        vertexBuffer[1] = start.y;
        vertexCount = 1;
    } else {
        for(i = 0; i < count; ++i) {
            if(vertexCount == vertexMax) {
                vertexMax = 2 * vertexMax;
                vertexBuffer = realloc(vertexBuffer, vertexMax * 2 * sizeof(GLfloat));
            }
            t = ((GLfloat)i) / ((GLfloat)count); // not count - 1, just leave the end point blank; the "1. *" is emitted.
            vertexBuffer[2 * vertexCount + 0] = quad(start, control, end, x, t);
            vertexBuffer[2 * vertexCount + 1] = quad(start, control, end, y, t);
            vertexCount += 1;
        }
    }
    drawVBO(vertexCount, vboId);
}


// Handles the start of a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[event touchesForView:self] enumerateObjectsUsingBlock:^(UITouch * obj, BOOL *stop) {
        if(![touches containsObject:obj])
            return;
        // for those touches that begins
        [ongoingTouches addObject:obj];
        CGPoint point  = CGPointScale([obj locationInView:self], _inverseScaleFactor);
        obj.stroke = [RBStroke strokeWithPoint:point];
        obj.strokeType = currentStrokeType;
        if(currentStrokeType == kRBEraser) {
            CGFloat radius =
                brushTexture.width / 2.    /* distance / 2 = radius, in OpenGL units (pixels)*/
                * kEraserTextureHaloFactor       /* because of the halo around the circle, the size of it is somehow smaller than the picture width */
                / self.contentScaleFactor  /* UIKit logical units */
                * _inverseScaleFactor      /* Application-wise units */
                ;
            [RBCanvasService.instance onEraserMovedTo:point
                                               radius:radius
                                 addIntersectionToSet:obj.intersection];
        }
    }];
    
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGFloat             height = self.bounds.size.height;
    
    [[event touchesForView:self] enumerateObjectsUsingBlock:^(UITouch * obj, BOOL *stop) {
        if(![touches containsObject:obj])
            return;
        
        // for those touches moving
        CGPoint point = CGPointScale([obj locationInView:self], _inverseScaleFactor);
        CGPoint startPoint, controlPoint, endPoint;
        if([obj.stroke addPoint:point
               returnStartPoint:&startPoint
                   controlPoint:&controlPoint
                       endPoint:&endPoint]) {
            RBQuadRenderer(RBQuadDrawer)(startPoint,
                                         controlPoint,
                                         endPoint);
            if(obj.strokeType == kRBEraser) {
                CGFloat radius =
                    brushTexture.width / 2.    /* distance / 2 = radius, in OpenGL units (pixels)*/
                    * kEraserTextureHaloFactor       /* because of the halo around the circle, the size of it is somehow smaller than the picture width */
                    / self.contentScaleFactor  /* UIKit logical units */
                    * _inverseScaleFactor      /* Application-wise units */
                    ;
                [RBCanvasService.instance onEraserAlongQuadraticCurveWithStartPoint:CGPointScale(startPoint, _scaleFactor)
                                                                       controlPoint:CGPointScale(controlPoint, _scaleFactor)
                                                                           endPoint:CGPointScale(endPoint, _scaleFactor)
                                                                             radius:radius
                                                               addIntersectionToSet:obj.intersection];

            }
        }
    }];
    
}

// Handles the end of a touch event when the touch is a tap.
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    CGFloat             height = self.bounds.size.height;
    
    [[event touchesForView:self] enumerateObjectsUsingBlock:^(UITouch * obj, BOOL *stop) {
        if(![touches containsObject:obj])
            return;
        
        // for those touches that ended
        [ongoingTouches removeObject:obj];
        CGPoint startPoint, endPoint;
        if([obj.stroke finishAndReturnStartPoint:&startPoint
                                        endPoint:&endPoint
                                    createBitmap:obj.strokeType == kRBBrush]) {
            RBLineRenderer(RBLineDrawer)(startPoint, endPoint);
        }
        switch (obj.strokeType) {
            case kRBBrush:
                [RBCanvasService.instance addStroke:obj.stroke];
                break;
            case kRBEraser:
                // remove the eraser path by redrawing the whole thing
                [RBCanvasService.instance redrawCurrentProfile];
                NSLog(@"[PaintingView] asyncRemove %d strokes", (int)obj.intersection.count);
                [RBCanvasService.instance asyncRemoveAll:obj.intersection.allObjects];
                break;
        }
        
    }];
}

// Handles the end of a touch event.
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// If appropriate, add code necessary to save the state of the application.
	// This application is not saving state.
    [[event touchesForView:self] enumerateObjectsUsingBlock:^(UITouch * obj, BOOL *stop) {
        if(![touches containsObject:obj])
            return;
        [ongoingTouches removeObject:obj];        
        // remove the eraser path by redrawing the whole thing
        [RBCanvasService.instance redrawCurrentProfile];
    }];
}

- (void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue
{
	// Update the brush color
    brushColor[0] = red * kBrushOpacity;
    brushColor[1] = green * kBrushOpacity;
    brushColor[2] = blue * kBrushOpacity;
    brushColor[3] = kBrushOpacity;
    
    if (initialized) {
        glUseProgram(program[PROGRAM_POINT].id);
        glUniform4fv(program[PROGRAM_POINT].uniform[UNIFORM_VERTEX_COLOR], 1, brushColor);
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void) setDrawingEnabled:(BOOL)enabled {
    self.userInteractionEnabled = enabled;
    self.multipleTouchEnabled = enabled;
}

- (void) redrawWithProfile:(RBCanvasProfile *)profile animated:(BOOL)animated {
    MAIN_THREAD_CHECK();
    [profile performOnAllStrokes:^(NSArray * strokes) {
        [self drawStrokes:strokes animated:animated shouldClear:YES];
    }];
}

- (void) drawStrokes:(NSArray *)strokes animated:(BOOL)animated {
    [self drawStrokes:strokes animated:animated shouldClear:NO];
}

- (void) drawStrokes:(NSArray *)strokes animated:(BOOL)animated shouldClear:(BOOL)erase {
    RBStrokeType prevType = currentStrokeType;
    CGFloat height = self.bounds.size.height;
    RBLineDrawerType lineDrawer = RBLineDrawer;
    RBQuadDrawerType quadDrawer = RBQuadDrawer;
    
    if(!animated) {
        prepareRender(context, viewFramebuffer);
        if(erase) {
            [self clearBuffer];
        }
        RBUseBrush();
        for(RBStroke *stroke in strokes) {
            [stroke drawPathUsingLineDrawer:lineDrawer quadDrawer:quadDrawer];
        }
        if(erase) {
            // then draw ongoing touches
            for(UITouch *touch in ongoingTouches) {
                RBStrokeType type = touch.strokeType;
                RBUseStrokeType(type);
                if(type != currentStrokeType)
                    continue;
                [touch.stroke drawPathUsingLineDrawer:lineDrawer quadDrawer:quadDrawer];
            }
        }
        displayBuffer(context, viewRenderbuffer);
    } else {
        RBLineDrawerType lineRenderer = RBLineRenderer(lineDrawer);
        RBQuadDrawerType quadRenderer = RBQuadRenderer(quadDrawer);
        if(erase) {
            [self erase];
        }
        RBUseBrush();
        for(RBStroke *stroke in strokes) {
            [stroke drawPathUsingLineDrawer:lineRenderer quadDrawer:quadRenderer];
        }
        if(erase) {
            // then draw ongoing touches
            for(UITouch *touch in ongoingTouches) {
                RBStrokeType type = touch.strokeType;
                RBUseStrokeType(type);
                if(type != currentStrokeType)
                    continue;
                [touch.stroke drawPathUsingLineDrawer:lineRenderer quadDrawer:quadRenderer];
            }
        }
    }
    // restore stroke type
    if(prevType == kRBEraser) {
        RBUseEraser();
    }
}

- (RBStrokeType) toggleBrushOrEraser {
    if(currentStrokeType == kRBEraser) {
        NSLog(@"using brush");
        RBUseBrush();
    } else if (currentStrokeType == kRBBrush) {
        NSLog(@"using eraser");
        RBUseEraser();
    }
    return currentStrokeType;
}

// http://stackoverflow.com/questions/8315912/opengl-es-buffer-in-ios

- (void)asyncDumpImageWithCanvasId:(NSString*)canvasId completion:(void(^)(UIImage *))completion {
    [self performSelectorInBackground:@selector(dumpImageWithArgs:)
                           withObject:completion];
}

- (void)dumpImageWithArgs:(void (^)(UIImage *))completion {
    
    // calculate buffer size
    NSInteger dataLength = backingWidth * backingHeight * 4;
    //    void *buffer = (GLubyte*)malloc(dataLength);
    GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        // transfer image from frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glReadPixels(0, 0, backingWidth, backingHeight, GL_RGBA, GL_UNSIGNED_BYTE, data);
    });
    
    // capture image
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef imageRef = CGImageCreate(backingWidth, backingHeight, 8, 32, backingWidth * 4, colorSpace, kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast, ref, NULL, true, kCGRenderingIntentDefault);
    
    UIImage *image;
    image = [[UIImage alloc] initWithCGImage:imageRef];
    image = [UIImage imageWithData:UIImagePNGRepresentation(image)];
    image = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];
    
    // capture image
    if( image == nil )
        NSLog(@"[PaintingView] FATAL: Save EAGLImage failed to bind data to a UIImage");
    else {
        
    }
    
    // clean up
    CGImageRelease(imageRef);
    CGColorSpaceRelease(colorSpace);
    CGDataProviderRelease(ref);
    free(data);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(image);
    });
}

@end
