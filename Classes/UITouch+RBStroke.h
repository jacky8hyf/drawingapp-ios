//
//  UITouch+RBStroke.h
//  drawingapp
//
//  Created by Elsk Hong on 7/6/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#ifndef drawingapp_UITouch_RBStroke_h
#define drawingapp_UITouch_RBStroke_h

#import <UIKit/UIKit.h>
#import "RBStroke.h"

@interface UITouch(RBStroke)
/*!
 * The RBStroke object that relates to this UITouch object.
 */
@property (atomic, readwrite, strong) RBStroke *stroke;
@property (atomic, readwrite) RBStrokeType strokeType;
@property (atomic, readonly, strong) NSMutableSet */*of RBStroke*/intersection;
@end

#endif
