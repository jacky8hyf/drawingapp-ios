//
//  NSArray+Utils.m
//  drawingapp
//
//  Created by Elsk Hong on 7/11/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSArray+Utils.h"

@implementation NSArray (RBUtils)

- (NSArray *)arrayByApplyingFunction:(id (^)(id))func
{
    id arr[self.count];
    for (NSUInteger i = 0; i < self.count; i++) {
        arr[i] = func([self objectAtIndex:i]);
    }
    return [NSArray arrayWithObjects:arr count:self.count];
}

@end