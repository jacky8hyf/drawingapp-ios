//
//  UITouch+RBStroke.m
//  drawingapp
//
//  Created by Elsk Hong on 7/6/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import <objc/runtime.h>
#import <Foundation/Foundation.h>
#import "UITouch+RBStroke.h"

@implementation UITouch(RBStroke)
@dynamic stroke;
- (void) setStroke:(RBStroke *)stroke {
    objc_setAssociatedObject(self, @"UITouch_RBStroke", stroke, OBJC_ASSOCIATION_RETAIN);
}
- (RBStroke *)stroke
{
    return objc_getAssociatedObject(self, @"UITouch_RBStroke");
}

@dynamic strokeType;
- (void) setStrokeType:(RBStrokeType)strokeType {
    objc_setAssociatedObject(self, @"UITouch_RBStrokeType", @(strokeType), OBJC_ASSOCIATION_RETAIN);
    if(strokeType == kRBEraser) {
        objc_setAssociatedObject(self, @"UITouch_intersection", [NSMutableSet new], OBJC_ASSOCIATION_RETAIN);
    }
}

- (RBStrokeType)strokeType {
    return [objc_getAssociatedObject(self, @"UITouch_RBStrokeType") intValue];
}

@dynamic intersection;
- (NSMutableSet */*of RBStroke*/)intersection {
    return objc_getAssociatedObject(self, @"UITouch_intersection");
}

@end