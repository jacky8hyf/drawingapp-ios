//
//  RBUserDefaultsService.h
//  drawingapp
//
//  Created by Elsk Hong on 7/11/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBUserDefaultsService : NSObject {
    // if nil then not loaded yet.
    NSMutableOrderedSet * /*of NSString*/ _canvasIds;
    NSUserDefaults * _userDef;
    id _mutex;
}

+ (instancetype) instance;

- (BOOL) toggleFavoriteOfCanvasId:(NSString *)canvasId error:(NSError **)error;
- (BOOL) isFavorite:(NSString *)canvasId error:(NSError **)error;
- (NSArray * /*of NSString*/ ) allFavoriteCanvasesWithError:(NSError **)error;

@end
