//
//  RBCanvasProfile.m
//  drawingapp
//
//  Created by Elsk Hong on 7/9/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import "RBCanvasProfile.h"
#import "AppController.h"
#import "RBCanvasService.h"

RB_INLINE BOOL
advanceTo(NSTimeInterval *old, const NSTimeInterval new) {
    if(*old < new) {
        *old = new;
        return YES;
    }
    return NO;
}

@implementation RBCanvasProfile

- (instancetype)initWithId:(NSString *)canvasId {
    self = [super init];
    if(self) {
        _canvasId = canvasId;
        _strokesIdsOnServer = [NSMutableSet new];
        _strokesOnScreen = [NSMutableArray new];
        _shouldEnableDrawing = NO;
        _lastCreatedAt= 0;
        _isFetching = NO;
        _shouldFetch = NO;
    }
    return self;
}

- (void)main:(NSNumber *)shouldToastIfSuccess
{
    NSError * error;
    // start listening changes.
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setObject:@[_canvasId] forKey:@"channels"];
    [currentInstallation save:&error];
    if(error) {
        if (AppController.sharedInstance.deviceTokenFetchState != kRBStateError) {
            // not on a simulator
            [RBCanvasService.instance onProfile:self
                                   addedContent:nil
                                          error:error
                           shouldToastIfSuccess:NO];
            return;
        }
        // else on a simulator, ignore it.
    }
    
    // assures that there is only one operation
    @synchronized(self) {
        if(_isFetching)
            return;
        _isFetching = YES;
    }
    NSTimeInterval time = _lastCreatedAt;
    NSArray * foundObjects;
    NSMutableArray * allDownloadedStrokes = [NSMutableArray new];
    NSUInteger offset = 0;
    do {
        @synchronized(self) {
            _shouldFetch = NO;
        }
        NSLog(@"[RBCanvasProfile] %@ should fetch...", _canvasId);
        AVQuery * query = [AVQuery queryWithClassName:RBLeanCloud_STROKE_CLASS_NAME];
        [query whereKey:@"canvasId" equalTo:_canvasId];
        [query whereKey:@"createdAt" greaterThan:[NSDate dateWithTimeIntervalSince1970:_lastCreatedAt]];
        [query orderByAscending:@"createdAt"];
        query.skip = offset;
        foundObjects = [query findObjects:&error];
        if(error) {
#warning FIXME on error, should flip isFetching back to NO
            [RBCanvasService.instance onProfile:self
                                   addedContent:nil
                                          error:error
                           shouldToastIfSuccess:NO];
            @synchronized(self) {
                _isFetching = NO;
            }
            return;
        }
        [foundObjects enumerateObjectsUsingBlock:^(AVObject *obj, NSUInteger idx, BOOL *stop) {
            NSString *objectId = obj.objectId;
            RBStroke *stroke = [RBStroke strokeFromPoints:[obj objectForKey:RBLeanCloud_STROKE_POINTS] withId:objectId];
            @synchronized(self) {
                if(![_strokesIdsOnServer containsObject:objectId]) {
                    [_strokesIdsOnServer addObject:objectId];
                    if(stroke) {
                        [allDownloadedStrokes addObject:stroke];
                        [_strokesOnScreen addObject:stroke];
                    }
                } // else ignore it.
            }
        }];
        NSLog(@"[RBCanvasProfile] found %d objects", (int)foundObjects.count);
        offset += foundObjects.count;
        if(foundObjects.count)
            advanceTo(&time, [[[foundObjects lastObject] createdAt] timeIntervalSince1970]);
        
        @synchronized(self) {
            BOOL b = _shouldFetch;
            _shouldFetch = NO;
            if(!foundObjects.count && !b)
                break;
        }
    } while(YES);
    
    _lastCreatedAt = time;
    _shouldEnableDrawing = YES; // the only place I flip shouldEnableDrawing (and will not flip it back)
    [RBCanvasService.instance onProfile:self
                           addedContent:allDownloadedStrokes
                                  error:nil
                   shouldToastIfSuccess:[shouldToastIfSuccess boolValue]];
    @synchronized(self) {
        _isFetching = NO;
    }
}

//NSOperationQueue * initQueue() {
//    static NSOperationQueue * queue;
//    static dispatch_once_t predicate;
//    dispatch_once(&predicate, ^{
//        queue = [NSOperationQueue new];
//        [queue setMaxConcurrentOperationCount:8];
//    });
//    return queue;
//}

+ (instancetype) openCanvas:(NSString*)canvasId {
//    NSOperationQueue * queue = initQueue();
    // create a profile
    RBCanvasProfile *p = [[RBCanvasProfile alloc] initWithId:canvasId];
    // dispatch an operation to initialize it
//    [queue addOperation:p];
    [p performSelectorInBackground:@selector(main:) withObject:@(YES)];
    // return the profile (not yet initialized)
    return p;
}

- (void) addStroke:(RBStroke *)stroke
{
    if(!stroke)
        @throw [NSException exceptionWithName:@"IllegalArgumentException" reason:@"nil stroke" userInfo:nil];
    if(!stroke.finished)
        @throw [NSException exceptionWithName:@"IllegalArgumentException"
                                       reason:@"stroke is not finished"
                                     userInfo:nil];
    @synchronized(self) {
        [_strokesOnScreen addObject:stroke];
    }
    
    AVObject __block *bean = [AVObject objectWithClassName:RBLeanCloud_STROKE_CLASS_NAME
                       dictionary:@{RBLeanCloud_STROKE_CANVAS_ID:_canvasId,
                                    RBLeanCloud_STROKE_POINTS:stroke.pointsArray}];
    [bean saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded) {            
            BOOL stillOnScreen = NO;
            @synchronized(self) {
                for(NSUInteger i = _strokesOnScreen.count - 1;
                    i < _strokesOnScreen.count;
                    i--) {
                    if([_strokesOnScreen objectAtIndex:i] == stroke) {
                        stillOnScreen = YES; break;
                    }
                }
            }
            if(stillOnScreen) { // normal case
                [_strokesIdsOnServer addObject:bean.objectId];
                [stroke setStrokeId:bean.objectId];
                [RBCanvasService.instance onProfile:self addedStrokeWithId:bean.objectId];
            } else { // erased before sent to server
                [self asyncRemoveAll:@[stroke]];
                [RBCanvasService.instance onProfileShouldRedraw:self];
            }
            
        } else {
            @synchronized(self) {
                for (NSUInteger i = _strokesOnScreen.count - 1;
                     i < _strokesOnScreen.count; // notice that i is unsigned -- common c trap!
                     i--) {
                    if([_strokesOnScreen objectAtIndex:i] == stroke) { // just compare the pointer
                        [_strokesOnScreen removeObjectAtIndex:i];
                        break;
                    }
                }
            }
            [RBCanvasService.instance onProfile:self cannotAddStrokeWithError:error];
        }
    }];
}

- (void) performOnAllStrokes:(void (^)(NSArray *))strokesDrawer {
#warning TODO this may be slow in locking.
    @synchronized(self) {
        strokesDrawer(_strokesOnScreen);
    }
}

- (void) asyncDownloadAndDrawStrokesTill:(NSTimeInterval)tillTime {
    @synchronized(self) {
        if(advanceTo(&_lastNotificationCreatedAt, tillTime)) {
            _shouldFetch = YES;
            if(!_isFetching) {
                [self performSelectorInBackground:@selector(main:) withObject:@(NO)];
            }
        }
    }
}

- (void) asyncRemoveAll:(NSArray * /*of RBStroke*/)strokes  {
    if(!strokes.count)
        return;
    @synchronized(self) {
        [_strokesOnScreen removeObjectsInArray:strokes];
        [self performSelectorInBackground:@selector(removeFromServer:) withObject:strokes];
    }
}

- (BOOL) onServerRemovedStrokeWithId:(NSString *)strokeId {
    if(!strokeId.length)
        return NO;
    BOOL removedFromScreen = NO;
    @synchronized(self) {
        // new strokes are likely to be erased, so count backwards.
        for (NSUInteger i = _strokesOnScreen.count - 1;
             i < _strokesOnScreen.count; // notice that i is unsigned -- common c trap!
             i--) {
            RBStroke *obj = [_strokesOnScreen objectAtIndex:i];
            if(obj.strokeId && [obj.strokeId isEqual:strokeId]) {
                [_strokesOnScreen removeObjectAtIndex:i];
                removedFromScreen = YES;
                break;
            }
        }
        // if nothing to be removed... well data was not pulled. so ignore it.
        [_strokesIdsOnServer removeObject:strokeId];
    }
    return removedFromScreen;
}

- (void) removeFromServer:(NSArray * /*of RBStroke*/) strokes {
    for(RBStroke *stroke in strokes) {
        NSString *strokeId = stroke.strokeId;
        if(!strokeId.length)
            continue;
        AVObject *bean = [AVObject objectWithoutDataWithClassName:RBLeanCloud_STROKE_CLASS_NAME objectId:strokeId];
        [bean deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(succeeded) {
                // ok :)
                [_strokesIdsOnServer removeObject:strokeId];
                [RBCanvasService.instance onProfile:self deletedStrokeWithId:strokeId];
            } else {
                // well draw it back on screen
                [_strokesOnScreen addObject:stroke];
                [RBCanvasService.instance onProfile:self cannotDeleteStroke:stroke error:error];
            }
        }];
    }
    
}


@end
