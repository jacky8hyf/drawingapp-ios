//
//  RBCanvasService.m
//  drawingapp
//
//  Created by Elsk Hong on 7/6/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import "RBCommon.h"
#import "RBCanvasService.h"
#import "RBUserDefaultsService.h"

#define RBShouldAnimate NO

@implementation RBCanvasService

SHARED_INSTANCE_ACCESS_METHOD(instance)

- (instancetype) init
{
    self = [super init];
    if(self) {
        _currentProfileMutex = [NSObject new];
        
    }
    
    return self;
}


- (void) dealloc
{
    //    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// ~--- MODEL changed -> change VIEW
- (void)onProfile:(RBCanvasProfile *)profile
                 addedContent:(NSArray *)strokes
                   error:(NSError *)error
    shouldToastIfSuccess:(BOOL)shouldToastIfSuccess
{
    
    NSLog(@"[RBCanvasService] Profile %@ content changed",
          profile.canvasId);
    @synchronized(_currentProfileMutex) {
        if(profile != _currentProfile)
            return;
        if(error) {
            NSLog(@"[RBCanvasProfileChangedProfile] error in profile content changed %@", error);
            RBToast([NSString stringWithFormat:RBToastCannotDownloadStrokesLocalizedFormat, profile.canvasId]);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_viewController drawStrokes:strokes animated:RBShouldAnimate];
                [_viewController setDrawingEnabled:profile.shouldEnableDrawing];
                if(shouldToastIfSuccess)
                    RBToast([NSString stringWithFormat:RBToastSuccessfullyDownloadedCanvasLocalizedFormat, profile.canvasId]);
            });
            
        }
        
    }
}

- (void) onProfileShouldRedraw:(RBCanvasProfile *)profile
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized(_currentProfileMutex) {
            if(_currentProfile == profile) {
                [_viewController redrawWithProfile:_currentProfile animated:NO];
            }
        }
    });
}

- (void) redrawCurrentProfile
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized(_currentProfileMutex) {
            [_viewController redrawWithProfile:_currentProfile animated:NO];
        }
    });
}

- (void) onProfile:(RBCanvasProfile *)profile addedStrokeWithId:(NSString *)strokeId
{
    NSLog(@"added stroke %@", strokeId);
}

- (void) onProfile:(RBCanvasProfile *)profile cannotAddStrokeWithError:(NSError*)error
{
    // No Internet = cannot upload data case.
    @synchronized(_currentProfileMutex) {
        if(profile != _currentProfile)
            return;
        if(error)
            NSLog(@"[RBCanvasProfile] error in adding stroke = %@", error);
        RBToast(RBToastCannotAddStrokeLocalized);
        [self onProfileShouldRedraw:profile]; // redraw no matter what
    }
}


- (void) onProfile:(RBCanvasProfile *)profile deletedStrokeWithId:(NSString *)strokeId {
    NSLog(@"deleted stroke %@", strokeId);
}
- (void) onProfile:(RBCanvasProfile *)profile cannotDeleteStroke:(RBStroke *)stroke error:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized(_currentProfileMutex) {
            if(profile != _currentProfile)
                return;
            if(error)
                NSLog(@"[RBCanvasProfile] error in deleting stroke = %@", error);
            RBToast(RBToastCannotDeleteStrokeLocalized);
            [_viewController drawStrokes:@[stroke] animated:NO];
        }
    });
}

// ~--- VIEW UI interactions -> change MODEL

- (void) openCanvas:(NSString *)canvasId
{
    MAIN_THREAD_CHECK();
    NSLog(@"[RBCanvasService] opening canvas %@", canvasId);
    @synchronized(_currentProfileMutex) {
        _currentProfile = [RBCanvasProfile openCanvas:canvasId];
        NSLog(@"[RBCanvasService] created profile %@, erasing...", _currentProfile.canvasId);
        RBToast([NSString stringWithFormat:RBToastLoadingCanvasLocalizedFormat,
                 _currentProfile.canvasId, nil]);
        [_viewController erase];
        [_viewController setDrawingEnabled:NO];
    }
}

- (void) addStroke:(RBStroke *)stroke
{
    // add it to MODEL
    @synchronized(_currentProfileMutex) {
        [_currentProfile addStroke:stroke];
    }
}

- (void) asyncRemoveAll:(NSArray * /*of RBStroke*/)array {
    // remove them from model
    @synchronized(_currentProfileMutex) {
        [_currentProfile asyncRemoveAll:array];
    }
}

- (void) onEraserAlongQuadraticCurveWithStartPoint:(CGPoint)start
                                      controlPoint:(CGPoint)control
                                          endPoint:(CGPoint)end
                                            radius:(CGFloat)radius
                              addIntersectionToSet:(NSMutableSet*/*of RBStroke*/)set {
    static dispatch_queue_t queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create("EraserQueue", DISPATCH_QUEUE_SERIAL);
    });
    
    dispatch_async(queue, ^{
        NSUInteger i, count;
        CGFloat t, xt, yt;
        @synchronized(_currentProfileMutex) {
            count = quadCount(start, control, end, radius * .8); // using radius * .8 as the step makes a good and efficient approx... maybe
            for(i = 0; i < count; i++) {
                t = ((CGFloat) i) / ((CGFloat) count);
                xt = quad(start, control, end, x, t);
                yt = quad(start, control, end, y, t);
                [self onEraserMovedTo:CGPointMake(xt, yt)
                               radius:radius
                 addIntersectionToSet:set];
            }
        }
    });
}

- (void) onEraserMovedTo:(CGPoint)center
                  radius:(CGFloat)radius
         addIntersectionToSet:(NSMutableSet*/*of RBStroke*/)set {
    NSMutableArray * /*of RBStroke*/intersection = [NSMutableArray new];
    @synchronized(_currentProfileMutex) {
        [_currentProfile performOnAllStrokes:^(NSArray * strokes) {
            for(RBStroke *obj in strokes) {
                if([obj intersectWithCircleAt:center radius:radius]) {
                    [intersection addObject:obj];
                }
            }
            if(intersection.count)
                NSLog(@"[RBCanvasService] %d strokes to be erased", (int)intersection.count);
        }];
        
//        [_currentProfile asyncRemoveAll:intersection];
        [set addObjectsFromArray:intersection];
    }
}

- (NSString *) toggleFavorite
{
    NSError *error;
    @synchronized(_currentProfileMutex) {
        BOOL res = [RBUserDefaultsService.instance
         toggleFavoriteOfCanvasId:_currentProfile.canvasId
         error:&error];
        if(error)
            return [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        return [NSString stringWithFormat:
                (res ? RBToastAddedToFavoriteLocalizedFormat : RBToastRemovedFromFavoriteLocalizedFormat),
                _currentProfile.canvasId, nil];
        ;
    }
}

- (void) asyncDownloadAndDrawStrokesTill:(NSTimeInterval)tillTime {
    @synchronized(_currentProfileMutex) {
        [_currentProfile asyncDownloadAndDrawStrokesTill:tillTime];
    }
}

- (void) onServerRemovedStrokeWithId:(NSString *)strokeId {
    @synchronized(_currentProfileMutex) {
        if([_currentProfile onServerRemovedStrokeWithId:strokeId])
            [self redrawCurrentProfile];
    }
}


- (NSString *) currentCanvasId
{
    @synchronized(_currentProfileMutex) {
        return _currentProfile.canvasId;
    }
}

- (void) setViewController:(PaintingViewController *)viewController
{
    _viewController = viewController;
}



@end
