//
//  RBCanvasProfile.h
//  drawingapp
//
//  Created by Elsk Hong on 7/9/15.
//  Copyright (c) 2015 Elsk Hong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RBStroke.h"

/*!
 * The MODEL in the MVC framework (that holds data).
 */
@interface RBCanvasProfile : NSOperation {
    /* final */ NSMutableSet * /*of NSString*/ _strokesIdsOnServer;
    /* final */ NSMutableArray * /*of RBStroke*/ _strokesOnScreen;
    NSTimeInterval _lastCreatedAt, _lastNotificationCreatedAt;
    BOOL _isFetching;
    BOOL _shouldFetch;
}

@property (readonly) /* final */ NSString *canvasId;
/*! The initialization state of this profile. */
@property (readonly) BOOL shouldEnableDrawing;

/*!
 * Open a RBCanvasProfile with the given canvasId. When done
 * -(void)[RBCanvasProfile onProfileChanged:content:error:shouldToastIfSuccess:]
 * will be called
 * After this method is called, the program will listen to push
 * messages in channel canvasId (and will stop listening other channels)
 */
+ (instancetype) openCanvas:(NSString*)canvasId;

/*!
 * Asynchrounously add stroke to server. If failed a
 * RBNotificationCannotAddStrokeName will be sent to default 
 * notification center; if successful a RBNotificationAddedStrokeName
 * will be sent.
 * @param stroke the stroke to add
 */
- (void) addStroke:(RBStroke *)stroke;

///*!
// * @deprecated
// */
//- (RBStroke *) addStrokeFromServer:(AVObject *)stroke;

///*!
// * Create a graphical view (UIImage) of all strokesOnScreen.
// * Precondition: size has positive width and height, and
// * stete = kRBStateSuccess.
// */
//- (UIImage *) createImageOfSize:(CGSize)size;

//- (void) drawOnContext:(CGContextRef)context;

//- (void) drawProfileUsingLineDrawer:(void (^)(CGPoint start, CGPoint end))lineDrawer
//                      quadDrawer:(void (^)(CGPoint start, CGPoint control, CGPoint end))quadDrawer;

- (void) performOnAllStrokes:(void (^)(NSArray *))strokesDrawer;
/*!
 * On local eraser erases strokes, modifies my data and send message to server.
 * This method DOES NOT change VIEW; caller should call "redraw profile" after
 * calling this method.
 */
- (void) asyncRemoveAll:(NSArray * /*of RBStroke*/)strokes ;

/*!
 * On receiving notification from server, pull data.
 */
- (void) asyncDownloadAndDrawStrokesTill:(NSTimeInterval)tillTime;
/*!
 * On receiving notification from server, erase certain strokes.
 */
- (BOOL) onServerRemovedStrokeWithId:(NSString *)strokeId;


@end
