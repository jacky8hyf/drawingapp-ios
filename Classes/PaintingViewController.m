/*
     File: PaintingViewController.m
 Abstract: The central controller of the application.
  Version: 1.13
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 
 */

#import <Toast/UIView+Toast.h>

#import "PaintingViewController.h"
#import "PaintingView.h"
#import "RBCanvasService.h"
#import "RBUserDefaultsService.h"
#import "SoundEffect.h"

typedef enum RBDialogType {
    kRBDialogTypeMainMenu,
    kRBDialogTypeMainMenuNoCanvas,
    kRBDialogTypeOpenCanvas,
    kRBDialogTypeAbout,
    kRBDialogTypeFavorites,
} RBDialogType;

//CONSTANTS:

// Padding for margins
#define kLeftMargin				10.0
#define kTopMargin				10.0
#define kRightMargin			10.0

#define kButtonSize             80.
#define kButtonGap              10.

//CLASS IMPLEMENTATIONS:

@interface PaintingViewController()
{
	CFTimeInterval		lastTime;
}
@end

@implementation PaintingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // additional view adjustments
    [self initMenuOptionKeys];
    [self setupButton:self.menuButton];
    [self setupPaintingView];
    [self.view addSubview:self.paintingView];
    [self.view bringSubviewToFront:self.menuButton];

    // now start to mess with controller and model
#if DEBUG
#else
    srand((unsigned)time(NULL)); // for random id generation.
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onToast:) name:RBNotificationToastName object:nil];
    [RBCanvasService.instance setViewController:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

// Release resources when they are no longer needed,
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dialog:actionSheet didDismissWithButtonIndex:buttonIndex];
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dialog:alertView didDismissWithButtonIndex:buttonIndex];
}

- (void) onToast:(NSNotification *)notification {
    dispatch_block_t block = ^{
        NSString * message = [notification.userInfo objectForKey:RBNotificationToastMessageKey];
        if(message.length)
            [self makeToast:message];
    };
    if([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

-(IBAction) onClick:(UIButton *)sender {
    if([sender isEqual:self.menuButton]) {
        NSString *canvasId = RBCanvasService.instance.currentCanvasId;
        NSArray *titleKeyArray;
        NSString *title;
        RBDialogType tag;
        if(canvasId.length) {
            titleKeyArray = _menuOptionKeys;
            title = [NSString stringWithFormat:RBDialogTitleCanvasIdLocalizedFormat,
                     RBCanvasService.instance.currentCanvasId, nil];
            tag = kRBDialogTypeMainMenu;
        } else {
            titleKeyArray = _menuOptionKeysNoCanvas;
            title = RBDialogTitleMenuLocalized;
            tag = kRBDialogTypeMainMenuNoCanvas;
        }
        NSArray *buttonTitles = [titleKeyArray arrayByApplyingFunction:
                                 ^NSString*(NSString *key) {
                                     return RBString(key);
                                 }];
        [[self createActionSheetWithTitle:title
                               withTag:tag
                         withButtonTitles:buttonTitles] showInView:self.view];
    }
}

// ~--- public methods called by RBCanvasService

#pragma mark -- Delegate methods of painting view.

- (void) drawStrokes:(NSArray *)strokes animated:(BOOL)animated {
    [self.paintingView drawStrokes:strokes animated:animated shouldClear:NO];
}

- (void) setDrawingEnabled:(BOOL)enabled {
    [self.paintingView setDrawingEnabled:enabled];
}

- (void) erase {
    [self.paintingView erase];
}

/*!
 * Redraw, ON CURRENT THREAD, the whole current profile.
 * this method returns after it finished drawing it.
 * Called by canvas service to make UI changes on a
 * profile change. Must be called on main thread.
 */
- (void) redrawWithProfile:(RBCanvasProfile *)currentProfile animated:(BOOL)animated{    
    MAIN_THREAD_CHECK();
    [self.paintingView redrawWithProfile:currentProfile animated:animated];
#warning TODO loads or unloads the progress bar
}

// ~--- helper methods

- (void) initMenuOptionKeys {
    _menuOptionKeys =
    [NSArray arrayWithObjects:
     RBMainMenuOpenCanvasKey,
     RBMainMenuToggleFavoritesKey,
     RBMainMenuFavoritesListKey,
     RBMainMenuShareKey,
     RBMainMenuToggleEraserKey,
     RBMainMenuHelpKey,
     RBMainMenuAboutKey, nil];
    _menuOptionKeysNoCanvas =
    [NSArray arrayWithObjects:
     RBMainMenuOpenCanvasKey,
     RBMainMenuFavoritesListKey,
     RBMainMenuHelpKey,
     RBMainMenuAboutKey, nil];
}

- (void) setupButton:(MKButton *)button {
    // storyboard ensures that it is a square.
    button.cornerRadius = button.frame.size.width / 2;
    button.backgroundLayerCornerRadius = button.frame.size.width / 2;
    button.maskEnabled = NO;
    button.ripplePercent = 1.75;
    //    self.menuButton.rippleLocation
    button.layer.shadowOpacity = 0.75;
    button.layer.shadowRadius = 3.5;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOffset = CGSizeMake(1.0, 5.5);
}

- (void) setupPaintingView {
    // painting view is added dynamically.
    CGRect rect;
    CGSize realSize = self.view.frame.size;
    CGFloat actualRatio = realSize.width / realSize.height;
    if(CGFloatAbs(actualRatio - RBBoardWHRatio) < 1e-3) {
        rect = self.view.frame;
    } else if (actualRatio > RBBoardWHRatio) { // too wide
        CGFloat wantedWidth = realSize.height * RBBoardWHRatio;
        rect = CGRectMake((realSize.width - wantedWidth) / 2, 0,
                          wantedWidth, realSize.height);
    } else { // too high
        CGFloat wantedHeight = realSize.width / RBBoardWHRatio;
        rect = CGRectMake(0, (realSize.height - wantedHeight) / 2,
                          realSize.width, wantedHeight);
    }
    
    self.paintingView = [[PaintingView alloc] initWithFrame:rect];
    
    // Defer to the OpenGL view to set the brush color
    [self.paintingView setBrushColorWithRed:1. green:1. blue:1.];
    [self.paintingView setDrawingEnabled:NO];
}

// dialog is either actionsheet or alertview
- (void)dialog:(id)dialog didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == [dialog cancelButtonIndex])
        return;
    switch([dialog tag]) {
        case kRBDialogTypeMainMenu:
        case kRBDialogTypeMainMenuNoCanvas:
            [self mainMenu:dialog didDismissWithButtonIndex:buttonIndex hasCanvas:[dialog tag] == kRBDialogTypeMainMenu];
            break;
        case kRBDialogTypeOpenCanvas:
            [self openCanvas:[dialog textFieldAtIndex:0].text];
            break;
        case kRBDialogTypeFavorites:
            [self openCanvas:[dialog buttonTitleAtIndex:buttonIndex]];
            break;
        case kRBDialogTypeAbout:
            if(buttonIndex != [dialog cancelButtonIndex])
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:RBHost]];
            break;
        default:
            break;
    }
}

- (void) mainMenu:(UIActionSheet *)menu
    didDismissWithButtonIndex:(NSInteger)buttonIndex
        hasCanvas:(BOOL)hasCanvas {
    NSArray * array = hasCanvas ? _menuOptionKeys : _menuOptionKeysNoCanvas;
    if(buttonIndex < 0 || buttonIndex >= array.count) {
        NSLog(@"[ViewController] FATAL: button %d is clicked", (int)buttonIndex);
        return; // wtf? ignore it
    }
    NSString *key = [array objectAtIndex:buttonIndex];
    if([key isEqualToString:RBMainMenuOpenCanvasKey]) {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:RBDialogTitleOpenCanvasLocalized
                               message:RBDialogInputCanvasIdLocalized
                               delegate:self
                               cancelButtonTitle:RBDialogCancelLocalized
                               otherButtonTitles:RBDialogOkLocalized,nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert textFieldAtIndex:0].text = RBCanvasService.instance.currentCanvasId;
        alert.tag = kRBDialogTypeOpenCanvas;
        [alert show];
    } else if ([key isEqualToString:RBMainMenuToggleFavoritesKey]) {
        NSString *message = [RBCanvasService.instance toggleFavorite];
        NSLog(@"%@", message);
        [self makeToast:message];
    } else if ([key isEqualToString:RBMainMenuFavoritesListKey]) {
        NSError *error;
        NSArray *arr = [RBUserDefaultsService.instance allFavoriteCanvasesWithError:&error];
        if(error) {
            NSLog(@"%@", error);
            [self makeToast:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
            return;
        }
        if (!arr.count) {
            NSLog(@"empty favorites list");
            [self makeToast:RBToastEmptyFavoriteListLocalized];
            return;
        }
        [[self createActionSheetWithTitle:RBDialogTitleFavoritesLocalized
                                  withTag:kRBDialogTypeFavorites
                         withButtonTitles:arr] showInView:self.view];
    } else if ([key isEqualToString:RBMainMenuShareKey]) {
        
        [self makeToast:RBToastDumpingImage];
        
        // http://iosdevelopertips.com/cocoa/launching-your-own-application-via-a-custom-url-scheme.html
        NSString *canvasId = [RBCanvasService.instance currentCanvasId];
        if(canvasId.length) {
            [self.paintingView asyncDumpImageWithCanvasId:canvasId completion:^(UIImage *img) {
                NSMutableArray *arr = [NSMutableArray arrayWithCapacity:5];
                NSURL *url = constructURL(canvasId);
                if(canvasId.length && url) {
                    [arr addObject:[NSString stringWithFormat:@"%@: %@", canvasId, url]];
                }
                
                if(img)
                    [arr addObject:img];
                if(!arr.count) {
                    // toast empty id message; FATAL ERROR!
                    NSLog(@"[ViewController] FATAL: nothing to be shared!");
                    return;
                }
                
                UIActivityViewController *vc =
                [[UIActivityViewController alloc] initWithActivityItems:arr applicationActivities:nil];
                // iOS 8 compatibility: http://www.cocoachina.com/bbs/read.php?tid=245969
                if ([vc respondsToSelector:@selector(popoverPresentationController)] ) {
                    vc.popoverPresentationController.sourceView = self.menuButton;
                }
                
                [self presentViewController:vc animated:true completion:nil];
            }];
        }
    } else if ([key isEqualToString:RBMainMenuHelpKey]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:RBHelpPage]];
    } else if ([key isEqualToString:RBMainMenuToggleEraserKey]) {
        if([self.paintingView toggleBrushOrEraser] == kRBBrush) {
            [self makeToast:RBToastUsingBrush];
        } else {
            [self makeToast:RBToastUsingEraser];
        }
    } else if ([key isEqualToString:RBMainMenuAboutKey]) {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:RBDialogTitleAboutLocalized
                               message:[NSString stringWithFormat:RBDialogAboutMessageLocalizedFormat, RBVersion()]
                               delegate:self
                               cancelButtonTitle:RBDialogCancelLocalized
                               otherButtonTitles:RBDialogOfficialWebsiteLocalized, nil];
        alert.tag = kRBDialogTypeAbout;
        [alert show];
    }
}

- (void) openCanvas:(NSString *)canvasId{
    if(!canvasId.length) {
        canvasId = [NSString stringWithFormat:@"%08x", RBRandom32BitInteger()];
    }
    [RBCanvasService.instance openCanvas:canvasId];
}

/*!
 * Shows an action sheet.
 * @return the action sheet
 */
- (UIActionSheet *)createActionSheetWithTitle:(NSString *)title
                                      withTag:(NSInteger)tag
                             withButtonTitles:(NSArray *)titles
{
    // action sheet has the nice property of
    // clicking outside = cancel, so use it instead of a alertView.
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:title
                                  delegate:self
                                  cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    [titles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [actionSheet addButtonWithTitle:obj];
    }];
    // hacky way for the seperation line
    [actionSheet addButtonWithTitle:RBDialogCancelLocalized];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    actionSheet.tag = tag;
    return actionSheet;
}

- (void)makeToast:(NSString *)message {
    [self.view makeToast:message];
}

@end
